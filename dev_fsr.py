'''
header order: 1. 'both' aka CEO, then directors, then officers. (all in order by last name)

PART I - (begins data)

going from plain to modified: if something is added, blue underlined. If removed, red
strike through


'''
import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import openpyxl
import sys
pd.options.mode.chained_assignment = None
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
np.seterr(all='raise')

class FSR:
    def __init__(self, p_df, m_df, ppl_df, questions_df, dict_of_dfs):
        self.p_df = p_df
        self.m_df = m_df
        self.ppl_df = ppl_df
        self.write_info = {}
        self.questions_df = questions_df
        self.dict_of_dfs = dict_of_dfs

        re_fileName = re.compile('(.*?)(_|plain|modified|final)+.*?\.csv', re.IGNORECASE)

        res = [f for f in os.listdir(os.getcwd()) if re.search('^[^~](.*?)(_|plain|modified|final)+.*?\.csv', f, re.IGNORECASE)]
        for r in res:
            if re_fileName.match(r) is not None:
                file_name = re_fileName.match(r).group(1)

        self.file_name = file_name
        self.workbook = xlsxwriter.Workbook('{}_Final Summary Report.xlsx'.format(file_name), {'nan_inf_to_errors': True})
        self.fsr_writer()
        # self.compare()

    # def compare(self):
        # pdf = pdf.apply(lambda row: re.sub('\s+', ' ', row).strip())
        # mdf = mdf.apply(lambda row: re.sub('\s+', ' ', row).strip())

        # difs = {}
        # column_nbrs = list(range(len(mdf.columns)))
        # for i in zip(mdf.index, pdf.index):
        #     if i[0] == i[1]:
        #         for c in zip(mdf.columns, pdf.columns):
        # #             print('index: ', i[0], 'column: ', c)
        #
        #             modVal = mdf.loc[i[0], (c[0],c[1])].values[0]
        #             plaVal = pdf.loc[i[1], (c[1],c[1])].values[0]
        # #             print(type(modVal))
        # #             print(modVal)
        # #             print(type(plaVal))
        # #             print(plaVal)
        #
        #             if isinstance(modVal, str):
        #                 modVal = re.sub('\s+', ' ', modVal).strip()
        #             if isinstance(plaVal, str):
        #                 plaVal = re.sub('\s+', ' ', plaVal).strip()
        # #             if modVal != plaVal:
        # #                 difs[(i[0], c[0])] = {'modified': modVal, 'plain': plaVal}
        # # #             if not isinstance(modVal, str):
        # #                 print('here!', modVal)

    #     count = 0
    #     for p_col, m_col in zip(self.p, self.m):
    #         #
    #         # if p_col != m_col:
    #         #     print(p_col, m_col)
    #         #
    #         for p_tuple, m_tuple in zip(self.p[p_col].iteritems(), self.m[m_col].iteritems()):
    #             if p_tuple[1] != m_tuple[1]:
    #                 print('cols', p_col, m_col)
    #                 print('indices', p_tuple[0], m_tuple[0])
    #                 print('values', p_tuple[1], m_tuple[1])
    #                 print('\n')
    #                 count+=1
    #                 # print(count)
    #                 if count == 22:
    #                     break
    #         if count == 22:
    #             break

    def fsr_writer(self):
        worksheet = self.workbook.add_worksheet(self.file_name[0:31])
        fmt = self.workbook.add_format({"font_name": "Times New Roman", 'font_size': 12, 'border': 1})
        last_col_fmt = self.workbook.add_format({"font_name": "Times New Roman", 'font_size': 12, 'top': 1, 'bottom': 1, 'left': 1, 'right': 2})
        bold_fmt = self.workbook.add_format({"font_name": "Times New Roman", 'bold': 1})
        bold_cntrgrey = self.workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'center_across': True, 'border': 1, 'top': 2, 'font_size': 12})
        bold_cntrgrey_titles = self.workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'center_across': True, 'border': 1, 'font_size': 12})
        right_fmt_bold_cntrgrey = self.workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'center_across': True, 'border': 1, 'font_size': 12, 'top': 2, 'right': 2})
        right_fmt_bold_cntrgrey_titles = self.workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'center_across': True, 'border': 1, 'font_size': 12, 'right': 2})
        bold_cntrgrey.set_align('center')
        bold_cntrgrey.set_align('vcenter')
        drk_gry_bar_hdr = self.workbook.add_format({'bg_color': '#808080', 'right': 2})
        salmon_bar_hdr = self.workbook.add_format({'align': 'center', "font_name": "Times New Roman", 'bold': 1, 'bg_color': '#fce4d6', 'right': 2, 'font_size': 14, 'top': 1, 'bottom': 1, 'left': 1, 'right': 2})
        salmon_bar_hdr.set_align('vcenter')
        indexfmt = self.workbook.add_format({"font_name": "Times New Roman", 'bg_color': '#fce4d6', 'border': 1})

        # write names
        colNum = 1
        for i in self.m_df.index.tolist():
            if i == self.m_df.index.tolist()[-1]:
                worksheet.write(0, colNum, i, right_fmt_bold_cntrgrey)
            else:
                worksheet.write(0, colNum, i, bold_cntrgrey)
            colNum += 1

        # write titles
        colNum = 1
        for i in self.m_df.index.tolist():
            value = self.ppl_df.loc[i]['Title']
            if i == self.m_df.index.tolist()[-1]:
                worksheet.write(1, colNum, value, right_fmt_bold_cntrgrey_titles)
            else:
                worksheet.write(1, colNum, value, bold_cntrgrey_titles)
            colNum += 1
        worksheet.merge_range(2, 1, 2, len(self.ppl_df['Capacity']), '', drk_gry_bar_hdr)

        # write index
        row = 3
        for question_id in self.questions_df.index:
            # print(self.questions_df.loc[question_id]['question'])
            if self.questions_df.loc[question_id]['header_symbol'] is not None:
                # print(self.questions_df.loc[question_id])
                # worksheet.write(row, 0, self.questions_df.loc[question_id]['header_symbol'], )
                worksheet.merge_range(row, 1, row, len(self.ppl_df['Capacity']), self.questions_df.loc[question_id]['header_symbol'], salmon_bar_hdr)
                row+=1
            if self.dict_of_dfs.get(question_id) is not None:
                worksheet.write(row, 0, self.questions_df.loc[question_id]['question_raw'], indexfmt)
                row+=1
                max = 0
                key = self.dict_of_dfs.get(question_id)
                for respondent_id in self.dict_of_dfs[question_id].keys():
                    if not self.dict_of_dfs[question_id][respondent_id].empty:
                        if len(self.dict_of_dfs[question_id][respondent_id]) > max:
                            biggest_df = self.dict_of_dfs[question_id][respondent_id]
                            max = len(biggest_df)
                print(biggest_df)
                for q in biggest_df['question'].tolist():
                    worksheet.write(row, 0, q, indexfmt)
                    row+=1
            else:
                worksheet.write(row, 0, self.questions_df.loc[question_id]['question_raw'], indexfmt)
                row+=1

    #     row = 4
    #     alphaHeaders = {}
    #     for c in self.m_df.columns:
    #         section_sensor = re.compile('^[A-Z]\.\s+(?!.*\(Continued\)).*')
    #         if section_sensor.match(c[1]):
    # #             section_name_with_number = re.compile('(^[A-Z]\.\s+.*)\d+\.\s+')
    #             section_name_with_number_and_content_between = re.compile('(^[A-Z]\.\s+[/A-Z0-9\(\)"\'\s\t\.-]+)[A-Z]?[a-z]+.*1\.\s+.*')
    #             section_name = section_name_with_number_and_content_between.match(c[1])
    #             if section_name is None:
    #                 section_name_with_number = re.compile('(^[A-Z]\.\s+[A-Z0-9\(\)/"\'\s\t\.-]+)\s+1\.\s+.*')
    #                 section_name = section_name_with_number.match(c[1])
    #             if section_name is not None:
    # #                 print('\nNumbered. Section: ', section_name.group(1))
    #                 section_name_with_question_numbered = re.compile('.*(1\..*)$')
    #                 question = section_name_with_question_numbered.match(c[1])
    # #                 print('Question: ', question.group(1))
    #             else:
    #                 section_name_with_no_number = re.compile('(^[A-Z]\.\s+.*)[A-Z]?[a-z]+.*')
    #                 section_name = section_name_with_no_number.match(c[1])
    #                 if section_name is not None:
    # #                     print('\nNo Number. Section: ', section_name.group(1))
    #                     question_with_no_number = re.compile('.*([A-Z][a-z].*)$')
    #                     question = question_with_no_number.match(c[1])
    # #                     print('Question: ', question.group(1))
    #                 else:
    #                     Warnings.warn('Houston, we have a problem')
    #
    #             alphaHeaders[row] = c
    #             worksheet.merge_range(row, 1, row, len(ppl_df['Capacity']), section_name.group(1), salmon_bar_hdr)
    #             row += 1
    # #             question = re.sub('(?<!^)\([ivx]+\)', '\n\n([ivx]+\)', question.group(1))
    # #             question = re.sub('(?<!^)\([a-b]{1,2}\)', '\n\n([a-b]{1,2}\)', question)
    #             worksheet.write(row, 0, question.group(1), indexfmt)
    #             row += 1
    #
            # else:
    #             formatted_index = re.sub('(?<!^)\([ivx]+', '\n\n(', c[1])
    #             print(formatted_index)
                # worksheet.write(row, 0,     , indexfmt)
                # row+=1
        # worksheet.set_column(0, 0, 55)

        # write values=
        # colNum = 1
        # rowNum = 5
        # for ind in self.m_df.index.tolist():
        #     for col in self.m_df.columns:
        #         if ind == self.m_df.index.tolist()[-1]:
        #             worksheet.write(rowNum, colNum, self.m_df.at[ind, col], last_col_fmt)
        #         else:
        #             worksheet.write(rowNum, colNum, self.m_df.at[ind, col], fmt)
        #         if rowNum + 1 in alphaHeaders:
        #             rowNum += 2
        #         else:
        #             rowNum += 1
        #     rowNum = 5
        #     colNum += 1
        self.workbook.close()

def opener(file):
    import csv
    from collections import OrderedDict
    # with open('columns.csv', encoding="utf8", mode='r') as csv_file:
    #     csv_reader = csv.reader(csv_file, delimiter=',')
    #     line_count = 0
    #     rows = []
    #     for row in csv_reader:
    #         line_count += 1
    #         rows.append(row)
    #     print(f'Processed {line_count} lines.')
    #     return rows
    try:
        columns = []
        with open(file, encoding = "utf8", mode='r') as f:
            reader = csv.reader(f)
            for row in reader:
                if columns:
                    row = [re.sub(r'\xa0', ' ', i) for i in row]
                    row = [re.sub('%%', '', i) for i in row]
                    row = [re.sub('\ufeff', '', i) for i in row]
                    for i, value in enumerate(row):
                        columns[i].append(value)
                else:
                    row = [re.sub(r'\xa0', ' ', i) for i in row]
                    row = [re.sub('%%', '', i) for i in row]
                    row = [re.sub('\ufeff', '', i) for i in row]
                    # first row
                    blank_column_count = 1
                    duplicate_column_count = 1
                    duplicate_tracker = []
                    for i, value in enumerate(row):
                        if value == '':
                            emptyColName = 'blank_column_{}'.format(blank_column_count)
                            row[i] = emptyColName
                            blank_column_count += 1
                            duplicate_tracker.append(emptyColName)
                        elif value in duplicate_tracker:
                            row[i] = value + '__{}'.format(duplicate_column_count)
                            duplicate_column_count += 1
                            duplicate_tracker.append(row[i])
                        else:
                            duplicate_tracker.append(value)
                            pass
                    columns = [[value] for value in row]
        as_dict = OrderedDict((subl[0], subl[1:]) for subl in columns)
        # print(as_dict.keys())
        # return as_dict
    except:
        columns = []
        with open(file, encoding = "ISO-8859-1", mode='r') as f:
            reader = csv.reader(f)
            for row in reader:
                if columns:
                    row = [re.sub(r'\xa0', ' ', i) for i in row]
                    row = [re.sub('%%', '', i) for i in row]
                    row = [re.sub('\ufeff', '', i) for i in row]
                    for i, value in enumerate(row):
                        columns[i].append(value)
                else:
                    row = [re.sub(r'\xa0', ' ', i) for i in row]
                    row = [re.sub('%%', '', i) for i in row]
                    row = [re.sub('\ufeff', '', i) for i in row]
                    # first row
                    blank_column_count = 1
                    duplicate_column_count = 1
                    duplicate_tracker = []
                    for i, value in enumerate(row):
                        if value == '':
                            emptyColName = 'blank_column_{}'.format(blank_column_count)
                            row[i] = emptyColName
                            blank_column_count += 1
                            duplicate_tracker.append(emptyColName)
                        elif value in duplicate_tracker:
                            row[i] = value + '__{}'.format(duplicate_column_count)
                            duplicate_column_count += 1
                            duplicate_tracker.append(row[i])
                        else:
                            duplicate_tracker.append(value)
                            pass
                    columns = [[value] for value in row]

        as_dict = OrderedDict((subl[0], subl[1:]) for subl in columns)
        # return as_dict
    # print('\n\n\n')
    # print(as_dict.keys())
    # print('\n\n\n')
    return as_dict

def ppl_parser(df):
    # Create dataframe of respondents
    ppl = dict()
    cols = ['ResponseID', 'UniqueIdentifier', 'FirstName', 'LastName', 'MiddleName', 'Capacity', 'Audit', 'Compensation', 'NomGov', 'Title']
    cols = ['UniqueIdentifier', 'FirstName', 'LastName', 'MiddleName', 'Capacity', 'Audit', 'Compensation', 'NomGov', 'Title']
    for k, v in df.items():
        if k in cols:
            ppl[k] = v

    ppl_df = pd.DataFrame.from_dict(ppl)

    # if (list(ppl_df.columns)[0] == '\ufeffUniqueIdentifier') | ((list(ppl_df.columns)[0] == '\ufeffResponseID')):
    #     newFirstCol = re.sub('\ufeff', '', list(ppl_df.columns)[0])
    #     ppl_df = ppl_df.rename(columns={list(ppl_df.columns)[0]: newFirstCol})

    # if 'ResponseID' in ppl_df.columns:
    #     ppl_df.set_index('ResponseID', inplace=True)
    # elif  'UniqueIdentifier' in ppl_df.columns:
    #     ppl_df.set_index('UniqueIdentifier', inplace=True)
    if  'UniqueIdentifier' in ppl_df.columns:
        ppl_df.set_index('UniqueIdentifier', inplace=True)

    orderedCapacities = ['Both', 'Director', 'Officer']
    df_list = []
    for i in orderedCapacities:
        d = ppl_df[ppl_df['Capacity']==i]
        d.sort_values(['LastName', 'FirstName'], inplace=True)
        df_list.append(d)
    ppl_df = pd.concat(df_list)

    return ppl_df

def mod_parser(dictionary):

    raw = pd.DataFrame.from_dict(dictionary)
    print('\nRaw Shape: ', raw.shape)

    # if (list(raw.columns)[0] == '\ufeffUniqueIdentifier') | ((list(raw.columns)[0] == '\ufeffResponseID')):
    #     newFirstCol = re.sub('\ufeff', '', list(raw.columns)[0])
    #     raw = raw.rename(columns={list(raw.columns)[0]: newFirstCol})

    # if 'ResponseID' in raw.columns:
    #     raw.set_index('ResponseID', inplace=True)
    # elif  'UniqueIdentifier' in raw.columns:
    #     raw.set_index('UniqueIdentifier', inplace=True)
    if  'UniqueIdentifier' in raw.columns:
        raw.set_index('UniqueIdentifier', inplace=True)
    columns = raw.columns

    # (?<!\(Continued\))$       [\t\s]*(a\.)?(1\.)?
    # firstCol_re = re.compile('^(?!.*Continued)(.*[\t\s]+|^)([AI1])[\.:].*')
    # firstCols = list(filter(firstCol_re.match, columns))
    # firstCol = firstCols[0]
    # symbol = re.search('.*[\t\s]+|^[(AI1)][\.:]', firstCol)
    # print(symbol.groups())
    # print('firstCols: ', len(firstCols))
    # print(firstCols)
    # return

    re_alpha_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(A)[\.:]')
    alpha_firstCols = list(filter(re_alpha_first_col.match, columns))
    re_roman_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(I)[\.:]')
    roman_firstCols = list(filter(re_roman_first_col.match, columns))
    re_arabic_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(1)[\.:]')
    arabic_firstCols = list(filter(re_arabic_first_col.match, columns))
    re_multipleWords_first_col = re.compile('\w+[\s\t]+\w+')
    multipleWords = list(filter(re_multipleWords_first_col.match, columns))

    first_col_list = []
    symbols = []
    if alpha_firstCols:
        alpha_firstCol = alpha_firstCols[0]
        alpha_symbol = re_alpha_first_col.match(alpha_firstCol).group(2)
        first_col_list.append(alpha_firstCol)
        symbols.append(alpha_symbol)
    if roman_firstCols:
        roman_firstCol = roman_firstCols[0]
        roman_symbol = re_roman_first_col.match(roman_firstCol).group(2)
        first_col_list.append(roman_firstCol)
        symbols.append(roman_symbol)
    if arabic_firstCols:
        arabic_firstCol = arabic_firstCols[0]
        arabic_symbol = re_arabic_first_col.match(arabic_firstCol).group(2)
        first_col_list.append(arabic_firstCol)
        symbols.append(arabic_symbol)
    if multipleWords:
        multipleWords_first_col = multipleWords[0]
        first_col_list.append(multipleWords_first_col)
        symbols.append('')

    # if len(symbols)==0:
        # Warnings.warn('No symbology found!')

    # print('\nAll Possible First Column Symbols Found: ', symbols)
    # print('Length of first_col_list=', len(first_col_list))
    # print('\nVariable: first_col_list:')
    # for col in first_col_list:
    #     print('\n')
    #     print(col)
    # print('\nEnd Col List\n------------------\n\n')

    for fc, sym in zip(first_col_list, symbols):
        if fc == first_col_list[0]:
            first_col_number = raw.columns.get_loc(fc)
            first_col_name = fc
            if sym != '':
                symbol = sym
        else:
            if raw.columns.get_loc(fc) < first_col_number:
                first_col_number = raw.columns.get_loc(fc)
                first_col_name = fc
                if sym != '':
                    symbol = sym
            else:
                continue

    # print('\nFirst Column Name:', first_col_name)
    # print('\nSymbol', symbol)
    # print('\n')
    # print('\n----------Questions:\n')
    data = raw.iloc[:, first_col_number:]
    questions_df = pd.DataFrame({'question_raw': data.columns})
    header_symbol_list = []
    questions = []
    sub_symbol_list = []
    # formatted_index = []
    for i, value in zip(questions_df.index, questions_df['question_raw']):
        if symbol == 'A':
            # section_sensor = re.compile('^(?!.*Continued)(?=(^|^[\t\s]*|^Part[\s\t]+)([A-Z]{1,2})[\.:][\t\s]*(a\.|1\.)?)')
            # if section_sensor.match(value):
            # section_name_with_number_and_content_between = re.compile('(?=(^|^[\t\s]*|^Part[\s\t]+)(^[A-Z]\.\s+[/A-Z0-9\(\)"\'\s\t\.-]+)[\t\s]*(_<)?[A-Z]?[a-z]+.*1\.\s+.*')
            # print(value)
            # (\d{1,2}[\.:]|[a-z]{1,2}[\.:].*)  (?=(^|^[\t\s]*|^Part[\s\t]+)(A)(\.|:)[\t\s]*(a\.|1\.)?)  [\t\s]*(a\.|1\.)?)(^.*)[\t\s]+([A-Z]{1,2}[\.:].*)[\s\t]+(\d+[\.:]|[ivx]+[\.:]|[a-z]{1,2}[\.:])
            re_sections = re.compile('^(?!.*\(Continued\))[\t\s]*(Part[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([A-Z]{1,2})(\.|:)(.*)')
            section_groups = re_sections.match(value)
            # print('\nvalue:', value)
            if section_groups is not None:
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                else:
                    header_symbol_list.append(None)
                questions.append(section_groups.group(5))
            else:
                    header_symbol_list.append(None)
                    questions.append(value)

                # print(section_groups.groups())
            # subsection = re.compile('')
    #         section_name = section_name_with_number_and_content_between.match(value)
    #         if section_name is None:
    #             section_name_with_number = re.compile('(^[A-Z]\.\s+[A-Z0-9\(\)/"\'\s\t\.-]+)\s+1\.\s+.*')
    #             section_name = section_name_with_number.match(value)
    #         if section_name is not None:
    # #                 print('\nNumbered. Section: ', section_name.group(1))
    #             section_name_with_question_numbered = re.compile('.*(1\..*)$')
    #             question = section_name_with_question_numbered.match(value)
    # #                 print('Question: ', question.group(1))
    #         else:
    #             section_name_with_no_number = re.compile('(^[A-Z]\.\s+.*)[A-Z]?[a-z]+.*')
    #             section_name = section_name_with_no_number.match(value)
    #             if section_name is not None:
    # #                     print('\nNo Number. Section: ', section_name.group(1))
    #                 question_with_no_number = re.compile('.*([A-Z][a-z].*)$')
    #                 question = question_with_no_number.match(c[1])
    # #                     print('Question: ', question.group(1))
    #             else:
    #                 Warnings.warn('Houston, we have a problem')
    #
    #             # alphaHeaders[row] = c
    #             # worksheet.merge_range(row, 1, row, len(ppl_df['Capacity']), section_name.group(1), salmon_bar_hdr)
    #             # row += 1
    # #             question = re.sub('(?<!^)\([ivx]+\)', '\n\n([ivx]+\)', question.group(1))
    # #             question = re.sub('(?<!^)\([a-b]{1,2}\)', '\n\n([a-b]{1,2}\)', question)
    #             # worksheet.write(row, 0, question.group(1), indexfmt)
    #             # row += 1
    #
    #         else:
    # #             formatted_index = re.sub('(?<!^)\([ivx]+', '\n\n(', c[1])
    # #             print(formatted_index)
    #             # worksheet.write(row, 0, c[1], indexfmt)
    #             # row+=1
    #             pass

        elif symbol == 'I':
            re_sections = re.compile('^(?!.*\(Continued\))[\t\s]*(Part[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([IVX]+)(\.|:)(.*)')
            section_groups = re_sections.match(value)
            if section_groups is not None:
                # print('\nvalue:', value)
                # print(section_groups.groups())
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                else:
                    header_symbol_list.append(None)
                questions.append(section_groups.group(5))
            else:
                header_symbol_list.append(None)
                questions.append(value)
                # re_question = re.compile('^(.*)(\b[\(\)\d]+[\.:\s\t]|\b[\(\)ivx]+[\.:\s\t]|\b[\(\)a-zA-Z]+[\.:\s\t])(.*)')
                # re_question = re.compile('^(.*)([\s\t]*[\(\)\d]+)[\.:\s\t](.*)')
                # question = re_question.match(value)
                # if question is not None:
                    # print(question.groups())
                    # header_symbol_list.append(None)
                    # questions.append(question.group(3))
                # else:
                    # print(question.groups())
        elif symbol == '1':
            re_sections = re.compile('^(?!.*\(Continued\))[\t\s]*(Part[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([\d]+)(\.|:)(?!\d)(.*)')
            section_groups = re_sections.match(value)
            if section_groups is not None:
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                else:
                    header_symbol_list.append(None)
                # print('\nvalue:', value)
                # print(section_groups.groups())
                questions.append(section_groups.group(5))
            else:
                header_symbol_list.append(None)
                questions.append(value)
                # re_question = re.compile('^(.*)(\b[\(\)\d]+[\.:\s\t]|\b[\(\)ivx]+[\.:\s\t]|\b[\(\)a-zA-Z]+[\.:\s\t])(.*)')
                # re_question = re.compile('^(.*)([\s\t]*[\(\)\d]+)[\.:\s\t](.*)')
                # question = re_question.match(value)
                # if question is not None:
                    # print(question.groups())
                    # header_symbol_list.append(None)
                    # questions.append(question.group(3))
                # else:
                    # print('ERROR!')
                    # print(question.groups())

        else:
            print('\nNO SYMBOL!\n')
    questions_df['header_symbol'] = header_symbol_list
    questions_df['question'] = questions
    # print(questions_df)
    # print('-------------------------\n\n')
    # Table question data

    dict_of_dfs = {}

    # print('SHAPE', data.shape, data.columns)


    # re_multiValued will not match the last occurence. As it stands, the raw data
    # will always contain one set of blank questions, so this is not an issue.
    # If you
    re_row_multiValued = re.compile('(.*?)(/(RowHeader)\d+:)(.*?);(?=[^;]*?(/RowHeader)\d+|$)')
    re_col_multiValued = re.compile('ColumnHeader\d+/(.*?:)(?=([^:]*?)(;ColumnHeader\d+/|;$))')
    type_flag = ''
    for q, i in zip(questions_df['question_raw'], questions_df.index):
        if (data[q].apply(lambda x: bool(re_row_multiValued.match(x))).any()) | (data[q].apply(lambda x: bool(re_col_multiValued.match(x))).any()):
            # max = pd.DataFrame()
            question_dict = {}
            for ind, row in zip(data.index, data[q]):
                print(ind, row=='')
                if re_row_multiValued.match(row) is not None:
                    multi = re_row_multiValued.findall(row)
                    type_flag = 'row'
                elif re_col_multiValued.match(row) is not None:
                    multi = re_col_multiValued.findall(row)
                    type_flag = 'col'
                elif row=='':
                    continue
                else:
                    warnings.warn('\n\n--------\nERROR\nCould not distinguish between col and row multiheaders')
                # print('\nFull Value: ', ind, row, '\n')
                individual_df = pd.DataFrame(columns=['question', 'value'])
                for ii, m in enumerate(multi):
                    if type_flag == 'row':
                        individual_df.loc[ii] = [m[0], m[3]]
                    elif type_flag == 'col':
                        individual_df.loc[ii] = [m[0], m[1]]
                    else:
                        print('ERROR')

                # Determine the largest of the individual_dfs
                # if len(max)==0:
                #     max = individual_df.copy()
                # else:
                #     if len(individual_df) > len(max):
                #         max = individual_df.copy()
                #     else:
                #         continue
                # if type_flag == 'col':
                #     print(ind)
                #     print(individual_df)
                # print('Unique Questions: ', individual_df['question'].nunique())
                # print(type_flag)
                # if individual_df['question'].nunique() == 4:
                # print(individual_df['question'].nunique())
                if type_flag == 'row':
                    if (individual_df['question'].nunique() != len(individual_df['question'])) and (individual_df.at[individual_df.index[-1], 'value'] in [None, '']):
                        # print('\n', ind)
                        # print(individual_df['question'].nunique() != len(individual_df['question']))
                        # print(individual_df['question'].nunique())
                        # print(individual_df)
                        individual_df.drop(individual_df.tail(individual_df['question'].nunique()).index,inplace=True)
                    pass
                elif type_flag == 'col':
                    # print(individual_df)
                    # individual_df.drop(individual_df.tail(individual_df['question'].nunique()).index,inplace=True)
                    # print(individual_df['value'].tolist()[-individual_df['question'].nunique():])
                    # if individual_df['question'].tolist()[-individual_df['question'].nunique():] == None:
                        # print('here!')
                    pass
                else:
                    print('ERROR')
                # print(ind)
                question_dict[ind] = individual_df
                # print('\nIndividual DF: ', individual_df)
                # print('Number of Unique Questions', individual_df['question'].nunique())
                # print('Unique Questions: ', individual_df['question'].unique())
            dict_of_dfs[i] = question_dict
            # if type_flag == 'col':
            #     print(dict_of_dfs)
    # for key in dict_of_dfs.keys():
    #     for key2 in dict_of_dfs[key].keys():
    #         if not dict_of_dfs[key][key2].empty:
    #             print(key, key2)
    #             print(dict_of_dfs[key][key2])
    #             print('\n\n')
    return data, questions_df, dict_of_dfs

    # print(max)
    # for key in dict_of_dfs.keys():
    #     for key2 in dict_of_dfs[key].keys():
    #         if not dict_of_dfs[key][key2].empty:
    #             print(key, key2)
    #             print(dict_of_dfs[key][key2])
    #             print('\n\n')
    #
    #         alphaHeaders[row] = c
    #         worksheet.merge_range(row, 1, row, len(ppl_df['Capacity']), section_name.group(1), salmon_bar_hdr)
    #         row += 1
    # #             question = re.sub('(?<!^)\([ivx]+\)', '\n\n([ivx]+\)', question.group(1))
    # #             question = re.sub('(?<!^)\([a-b]{1,2}\)', '\n\n([a-b]{1,2}\)', question)
    #         worksheet.write(row, 0, question.group(1), indexfmt)
    #         row += 1
    #
    #     else:
    # #             formatted_index = re.sub('(?<!^)\([ivx]+', '\n\n(', c[1])
    # #             print(formatted_index)
    #         worksheet.write(row, 0, c[1], indexfmt)
    #         row+=1
    # # for c in data.columns:
    # #     print(type(c))
    # return data
    # the_first_data_col =
    # if not roman_firstCols:
    #     header_format = 'arabic'
    #     firstCol = arabic_firstCols[0]
    #     print(firstCol)
    #     symbol = re_arabic_first_col.match(firstCol).group(2)
    #     print(symbol)
    # else:
    #     header_format = 'roman'
    #     firstCol = roman_firstCols[0]
    #     print(firstCol)
    #     symbol = re_roman_first_col.match(firstCol).group(2)
    #     print(symbol)
    # else:
    #     header_format = 'alpha'
    #     firstCol = alpha_firstCols[0]
    #     print(firstCol)
    #     symbol = re_alpha_first_col.match(firstCol).group(2)
    #     print(symbol)

    # re_arabic_first_col = re.compile('(?=(^1\.|^[\s\t]+)1\.(\t+|\s+|a\.))[/A-Z0-9\(\)"\'\s\t\.-]+[\s\t]+[A-Z]?[a-z]+.*')
    # re_arabic_first_col = re.compile('(?=(^1\.|^[\s\t]+1\.)).*1(\t+|\s+|(a\.)?|(1\.)?)[/A-Z0-9\(\)"\'\s\t\.-]+[\s\t]+[A-Z]?[a-z]+.*')
    # arabic_firstCols = list(filter(re_arabic_first_col.match, columns))


    # firstCol_upperFollowedByLowerOnly = re.compile('(?!.*[A-Z]\.\s+)(?!.*[IVX]+\.\s+)(?!.*\d+\.\s+)^[/A-Z0-9\(\)"\'\s\t\.-]+\s[A-Z]?[a-z]+.*')
    # upperFollowedByLowerOnly_firstCols = list(filter(firstCol_upperFollowedByLowerOnly.match, columns))
    #
    # re_multipleWords_first_col = re.compile('\w+[\s\t]+\w+')
    # multipleWords = list(filter(re_multipleWords_first_col.match, columns))
    # print('\nmultiple words', multipleWords)

    # firstCol_onlyNumbers = re.compile('(?=(^|.*\s+)1\.(\s+|a\.))')
    # onlyNumbers_noSectionHeaders = list(filter(firstCol_onlyNumbers.match, columns))

    # if multipleWords and alpha_firstCols:
    #     if raw.columns.get_loc(multipleWords[0]) < raw.columns.get_loc(alpha_firstCols[0]):
    #         first_column = raw.columns.get_loc(multipleWords[0])
    #     else:
    #         first_column = raw.columns.get_loc(alpha_firstCols[0])
    # elif multipleWords and roman_firstCols:
    #     if raw.columns.get_loc(multipleWords[0]) < raw.columns.get_loc(roman_firstCols[0]):
    #         first_column = raw.columns.get_loc(multipleWords[0])
    #     else:
    #         first_column = raw.columns.get_loc(roman_firstCols[0])
    # elif multipleWords and arabic_firstCols:
    #     if raw.columns.get_loc(multipleWords[0]) < raw.columns.get_loc(arabic_firstCols[0]):
    #         first_column = raw.columns.get_loc(multipleWords[0])
    #     else:
    #         first_column = raw.columns.get_loc(arabic_firstCols[0])
    # elif multipleWords and (not alpha_firstCols) and (not roman_firstCols) and (not arabic_firstCols):
    #     first_column = raw.columns.get_loc(multipleWords[0])
    # elif alpha_firstCols and (not roman_firstCols) and (not arabic_firstCols):
    #     first_column = raw.columns.get_loc(alpha_firstCols[0])
    # elif roman_firstCols and (not alpha_firstCols) and (not arabic_firstCols):
    #     first_column = raw.columns.get_loc(roman_firstCols[0])
    # elif arabic_firstCols and (not alpha_firstCols) and (not roman_firstCols):
    #     first_column = raw.columns.get_loc(arabic_firstCols[0])
    # else:
    #     Warnins.warn('\nCould not parse first column!\nMake sure that section headers conform to convention or that there are no headers, only numbers.\n')
    #
    # return raw.iloc[:,first_column:]


if __name__ == "__main__":
    import time
    start_time = time.time()
    print(sys.version)
    print('\n------------------------------------------\n')
    # parse(df)
    cwd = os.getcwd()
    plain = re.compile('^[^~].*plain.*\.csv', re.IGNORECASE)
    modified = re.compile('^[^~].*modified.*\.csv', re.IGNORECASE)

    for file in os.listdir(cwd):
        if modified.match(file):
            # mdf_raw = pd.read_csv(file, na_filter = False, na_values='', dtype=str)
            print('\n-----------------\nProccessing Modified...')
            print('Modified File Name: ', file)
            mdf_raw = opener(file)
            mdf, mquestions_df, mdict_of_dfs  = mod_parser(mdf_raw)
    for file in os.listdir(cwd):
        if plain.match(file):
            # pdf_raw = pd.read_csv(file, na_filter = False, na_values='', dtype=str)
            print('\n\n\n-----------------\nProcessing Plain...')
            print('Plain File Name: ', file)
            pdf_raw = opener(file)
            pdf, pquestions_df, pdict_of_dfs = mod_parser(pdf_raw)
    print('\n')
    ppl_df = ppl_parser(pdf_raw)

    writer = pd.ExcelWriter('dfs.xlsx')
    ppl_df.to_excel(writer, 'ppl_df')
    mdf.to_excel(writer, 'mdf')
    pdf.to_excel(writer, 'pdf')
    questions_df.to_excel(writer, 'questions')
    writer.save()

    if mquestions_df == pquestions_df:
        print('Equivalent Question Indices: ', mquestions_df == pquestions_df)

    fsr = FSR(pdf, mdf, ppl_df, questions_df, pdict_of_dfs)
    # print(ppl_df)
    # # print(list(set(list(pdf_raw.columns)) - set(list(mdf_raw.columns))))
    # columns = list(mdf_raw.columns)
    # mdf_raw.columns = columns
    # columns = list(pdf_raw.columns)
    # columns = [re.sub(r'\xa0', ' ', c) for c in columns]
    # columns = [re.sub('%%', '', c) for c in columns]
    # pdf_raw.columns = columns
    # # pdf_raw.columns = [[str(i) for i in list(range(len(pdf_raw.columns)))], pdf_raw.columns]
    # # pdf_raw.columns = ['___'.join(col) for col in pdf_raw.columns]
    # print('\nplain shape: ', pdf_raw.shape)
    # print('modified shape: ', mdf_raw.shape)
    #
    # pdf_raw.set_index('FullName', inplace=True)
    # mdf_raw.set_index('FullName', inplace=True)
    # # print('bool test', mdf_raw.index.tolist() == pdf_raw.index.tolist())
    # columns = pdf_raw.columns
    #
    # re_alpha_first_col = re.compile('(?=(^|.*\s+)A\.(\s+|a\.))')
    # alpha_firstCols = list(filter(re_alpha_first_col.match, columns))
    #
    # re_roman_first_col = re.compile('(?=(^|.*\s+)I\.(\s+|a\.))')
    # roman_firstCols = list(filter(re_roman_first_col.match, columns))
    #
    # #
    # re_arabic_first_col = re.compile('(?=(^1\.|^[\s\t]+)1\.(\t+|\s+|a\.))[/A-Z0-9\(\)"\'\s\t\.-]+[\s\t]+[A-Z]?[a-z]+.*')
    # arabic_firstCols = list(filter(re_arabic_first_col.match, columns))
    #
    #
    #
    # if arabic_firstCols:
    #     print(arabic_firstCols)
    #     print('\nyas\n')
    #
    # firstCol_upperFollowedByLowerOnly = re.compile('(?!.*[A-Z]\.\s+)(?!.*[IVX]+\.\s+)(?!.*\d+\.\s+)^[/A-Z0-9\(\)"\'\s\t\.-]+\s[A-Z]?[a-z]+.*')
    # upperFollowedByLowerOnly_firstCols = list(filter(firstCol_upperFollowedByLowerOnly.match, columns))
    #
    # firstCol_onlyNumbers = re.compile('(?=(^|.*\s+)1\.(\s+|a\.))')
    # onlyNumbers_noSectionHeaders = list(filter(firstCol_onlyNumbers.match, columns))
    #
    # if upperFollowedByLowerOnly_firstCols and alpha_firstCols:
    #     if pdf_raw.columns.get_loc(upperFollowedByLowerOnly_firstCols[0]) < pdf_raw.columns.get_loc(alpha_firstCols[0]):
    #         first_column = pdf_raw.columns.get_loc(upperFollowedByLowerOnly_firstCols[0])
    #     else:
    #         first_column = pdf_raw.columns.get_loc(alpha_firstCols[0])
    # elif upperFollowedByLowerOnly_firstCols and roman_firstCols:
    #     if pdf_raw.columns.get_loc(upperFollowedByLowerOnly_firstCols[0]) < pdf_raw.columns.get_loc(roman_firstCols[0]):
    #         first_column = pdf_raw.columns.get_loc(upperFollowedByLowerOnly_firstCols[0])
    #     else:
    #         first_column = pdf_raw.columns.get_loc(roman_firstCols[0])
    # elif upperFollowedByLowerOnly_firstCols and onlyNumbers_noSectionHeaders:
    #     if pdf_raw.columns.get_loc(upperFollowedByLowerOnly_firstCols[0]) < pdf_raw.columns.get_loc(onlyNumbers_noSectionHeaders[0]):
    #         first_column = pdf_raw.columns.get_loc(upperFollowedByLowerOnly_firstCols[0])
    #     else:
    #         first_column = pdf_raw.columns.get_loc(onlyNumbers_noSectionHeaders[0])
    # elif upperFollowedByLowerOnly_firstCols and (not alpha_firstCols) and (not roman_firstCols) and (not onlyNumbers_noSectionHeaders):
    #     first_column = pdf_raw.columns.get_loc(upperFollowedByLowerOnly_firstCols[0])
    # elif alpha_firstCols and (not roman_firstCols) and (not onlyNumbers_noSectionHeaders):
    #     first_column = pdf_raw.columns.get_loc(alpha_firstCols[0])
    # elif roman_firstCols and (not alpha_firstCols) and (not onlyNumbers_noSectionHeaders):
    #     first_column = pdf_raw.columns.get_loc(roman_firstCols[0])
    # elif onlyNumbers_noSectionHeaders and (not alpha_firstCols) and (not roman_firstCols):
    #     first_column = pdf_raw.columns.get_loc(onlyNumbers_noSectionHeaders[0])
    # else:
    #     Warnins.warn('\nCould not parse first column!\nMake sure that section headers conform to convention or that there are no headers, only numbers.\n')

    # pplwriter = pd.ExcelWriter('ppl_df.xlsx')
    # ppl_df.to_excel(pplwriter)
    # pplwriter.save()
    # writer2 = pd.ExcelWriter('pdf.xlsx')
    # pdf.to_excel(writer2)
    # writer2.save()

    # writer1 = pd.ExcelWriter('mdf.xlsx')
    # mdf.to_excel(writer1)
    # writer1.save()
    # writer2 = pd.ExcelWriter('pdf.xlsx')
    # pdf.to_excel(writer2)
    # writer2.save()
    print("\n\n--- %s seconds ---" % (time.time() - start_time))
