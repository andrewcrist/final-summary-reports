import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import sys
import itertools
import difflib
import logging
import time
logging.basicConfig(level=os.environ.get("WARNING", "INFO"))

pd.options.mode.chained_assignment = None
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
np.seterr(all='raise')

import traceback
# import warnings
# import sys
#
# def warn_with_traceback(message, category, filename, lineno, file=None, line=None):
#
#     log = file if hasattr(file,'write') else sys.stderr
#     traceback.print_stack(file=log)
#     log.write(warnings.formatwarning(message, category, filename, lineno, line))
#
# warnings.showwarning = warn_with_traceback

class FSR:
    '''
    Methods:

    * ask_user_plain_or_mod asks whether an fsr is generated without redlining for
        either the plain or modified files. Currently only in use for modified files
    * write_df prints data to workbook
    * write_dfs_in_dict may be used to show matrix questions in a format useful
        for spotting errors
    * align_groups_of_matrix_questions is legacy code
    * compare_indices compares shape data in the modified and plain files
    * write_names_and_titles writes names and titles in the workbook
    * write_row_index writes questions to the workbook
    * get_opcodes shows more diff information
    * get_diff_with_color style controller using diff information
    * write_data writes all the raw data to the workbook
    * fsr_handler controls the sequence of methods
    '''
    def __init__(self, p_df, m_df, ppl_df, questions_df, plain_dict_of_dfs, mod_dict_of_dfs, print_headers):
        self.p_df = p_df
        self.m_df = m_df
        self.ppl_df = ppl_df
        self.questions_df = questions_df
        self.plain_dict_of_dfs = plain_dict_of_dfs
        self.mod_dict_of_dfs = mod_dict_of_dfs
        self.print_headers = print_headers
        self.write_titles = True
        self.ppl_ids_set = set()

        re_fileName = re.compile('(.*?)(_|plain|modified|final)+.*?\.csv', re.IGNORECASE)

        res = [f for f in os.listdir(os.getcwd()) if re.search('^[^~](.*?)(_|plain|modified|final)+.*?\.csv', f, re.IGNORECASE)]
        for r in res:
            if re_fileName.match(r) is not None:
                file_name = re_fileName.match(r).group(1)

        self.file_name = file_name
        self.workbook = xlsxwriter.Workbook('{}_Final Summary Report.xlsx'.format(file_name), {'nan_inf_to_errors': True})
        self.fsr_handler()

    def ask_user_plain_or_mod(self, type):
        if type == 'plain':
            check = str(input("Would you like to produce a plain instance of the FSR? (y/n): ")).lower().strip()
        else:
            check = str(input("Would you like to produce a modified instance of the FSR? (y/n): ")).lower().strip()

        try:
            if check[0] == 'y':
                if type=='plain':
                    self.plain_workbook = xlsxwriter.Workbook('{}_Final Summary Report_PLAIN.xlsx'.format(self.file_name), {'nan_inf_to_errors': True})
                else:
                    self.mod_workbook = xlsxwriter.Workbook('{}_Final Summary Report_MODIFIED.xlsx'.format(self.file_name), {'nan_inf_to_errors': True})
                return True
            elif check[0] == 'n':
                return False
            else:
                print('Invalid Input')
                return ask_user()
        except Exception as error:
            print("Please enter a valid input")
            print(error)
            return self.ask_user_plain_or_mod(type)

    def write_df(self, df, sheetName):
        worksheet = self.workbook.add_worksheet(sheetName)
        df.fillna('', inplace=True)
        colNum = 1
        worksheet.write_row(0, 1, df.columns)
        worksheet.write_column(1, 0, df.index)

        for col in df.columns:
            worksheet.write_column(1, colNum, df[col])
            colNum += 1

    def write_dfs_in_dict(self, d1, d2, sheetName):
        worksheet = self.workbook.add_worksheet(sheetName)
        bold_fmt = self.workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'text_wrap': True})
        fmt = self.workbook.add_format({"font_name": "Times New Roman", 'text_wrap': True})

        # Ensure that pairwise keys in each dictionary exist. This loop ensures
        # that each dictionary is written in the same order, even when an item
        # does not occur in one or the other.
        for ppl_id in self.m_df.index:
            for q_id in self.questions_df.index:
                if (d1.get(q_id) is not None) or (d2.get(q_id) is not None):
                    plain_dict_item = d1.get(q_id)
                    plain_subframe= plain_dict_item.get(ppl_id)
                    mod_dict_item = d2.get(q_id)
                    mod_subframe = mod_dict_item.get(ppl_id)
                    if plain_subframe is None and mod_subframe is None:
                        continue
                    # Fill out the plain_subframe if it's None
                    elif plain_subframe is None and mod_subframe is not None:
                        plain_subframe = pd.DataFrame(index=mod_subframe.index, columns=mod_subframe.columns)
                        # plain_subframe.fillna('', inplace=True)
                        d1[q_id][ppl_id] = plain_subframe
                    # Fill out the mod_subframe if it is None
                    elif plain_subframe is not None and mod_subframe is None:
                        mod_subframe = pd.DataFrame(index=plain_subframe.index, columns=mod_subframe.columns)
                        # mod_subframe.fillna('', inplace=True)
                        d2[q_id][ppl_id] = mod_subframe

        row = 1
        count = 1
        worksheet.write(0, 0, 'PLAIN', bold_fmt)
        for q_id in self.questions_df.index:
            for person in self.m_df.index:
                if d1.get(q_id) is not None or d2.get(q_id) is not None:
                    if d1[q_id][person].empty:
                        row += len(d2[q_id][person])
                        continue
                    else:
                        worksheet.write(row, 0, q_id, bold_fmt)
                        worksheet.write(row, 1, self.questions_df.loc[q_id]['question_raw'], bold_fmt)
                        worksheet.write(row, 2, person, bold_fmt)
                        row+=1
                        worksheet.write(row, 1, 'question_id', bold_fmt)
                        worksheet.write(row, 2, 'question_value', bold_fmt)
                        worksheet.write(row, 3, 'answer_value', bold_fmt)
                        row+= 1

                        worksheet.write_column(row, 1, d1[q_id][person].index, fmt)
                        colNum = 2
                        for col in d1[q_id][person].columns:
                            worksheet.write_column(row, colNum, d1[q_id][person][col], fmt)
                            colNum += 1
                        row += len(d1[q_id][person]) + 1

                        if d2[q_id].get(person) is not None:
                            if len(d2[q_id][person]) > len(d1[q_id][person]):
                                # print('mod is bigger')
                                # print(len(d1[q_id][i]), len(d2[q_id][i]))
                                rowSkipCount = len(d2[q_id][person]) - len(d1[q_id][person])
                                # print(rowSkipCount)
                                row += rowSkipCount
                                worksheet.write(row, 0, 'here', fmt)
                            count += 1

        row = 1
        count = 1
        worksheet.write(0, 4, 'MODIFIED', bold_fmt)
        for q_id in self.questions_df.index:
            for person in self.m_df.index:
                if d1.get(q_id) is not None or d2.get(q_id) is not None:
                    if d2[q_id][person].empty:
                        row += len(d1[q_id][person])
                        continue
                    else:
                        colNum = 6

                        worksheet.write(row, 4, q_id, bold_fmt)
                        worksheet.write(row, 5, self.questions_df.loc[q_id]['question_raw'], bold_fmt)
                        worksheet.write(row, 6, person, bold_fmt)
                        row+=1
                        worksheet.write(row, 5, 'question_id', bold_fmt)
                        worksheet.write(row, 6, 'question_value', bold_fmt)
                        worksheet.write(row, 7, 'answer_value', bold_fmt)
                        row+= 1
                        # worksheet.write_row(row, 1, d1[q_id][i].columns)
                        # row+=1
                        worksheet.write_column(row, 5, d2[q_id][person].index, fmt)

                        for col in d2[q_id][person].columns:
                            worksheet.write_column(row, colNum, d2[q_id][person][col], fmt)
                            colNum += 1
                        row += len(d2[q_id][person]) + 1
                        if d1[q_id].get(person) is not None:
                            if len(d1[q_id][person]) > len(d2[q_id][person]):
                                # print('\nplain is bigger')
                                # print(len(d1[q_id][i]), len(d2[q_id][i]))
                                rowSkipCount = len(d1[q_id][person]) - len(d2[q_id][person])
                                # print(rowSkipCount)
                                row += rowSkipCount
                            count += 1
                        # break
                    # break


        for c in [0, 2, 3, 4, 6, 7, 8]:
            worksheet.set_column(c, c, 35)
        for c in [1, 5]:
            worksheet.set_column(c, c, 70)

    def align_groups_of_matrix_questions(self, plain_subframe, mod_subframe):
        #######################
        # Old
        # print('\n----------\nOriginals:\n',plain_subframe)
        # print(mod_subframe)

        # outer = pd.merge(mod_subframe, plain_subframe, on=['question', 'value'], how='outer').reset_index()
        # print(outer)
        # new_df_index = new_df.sort_values(by=['group_y', 'index'], na_position='last').index.tolist()
        # print(new_df_index)
        # print(plain_subframe)
        # plain_subframe = plain_subframe.reindex(new_df_index)
        # print(plain_subframe)
        # plain_subframe.reset_index(inplace=True, drop=True)
        # plain_subframe.fillna('', inplace=True)
        # print(plain_subframe)
        # print(mod_subframe)

        ##########################################
        # Working version

        mod_subframe.reset_index(inplace=True,drop=False)
        mod_subframe = mod_subframe.rename(columns={'index': 'mod_index', 'group': 'mod_group'})
        plain_subframe.reset_index(inplace=True,drop=False)
        plain_subframe = plain_subframe.rename(columns={'index': 'plain_index', 'group': 'plain_group'})
        inner = pd.merge(mod_subframe, plain_subframe, on=['question', 'value'], how='inner')
        # print(inner)
        # print(max(inner.groupby('group_x').agg({'question': 'nunique'})['question']))
        clean_inner = pd.DataFrame()
        partial_inner = pd.DataFrame()
        for g in inner['mod_group'].unique():
            if len(inner[inner['mod_group']==g]) == max(inner.groupby('mod_group').agg({'question': 'nunique'})['question']):
                clean_inner=clean_inner.append(inner[inner['mod_group']==g])
            else:
                partial_inner=partial_inner.append(inner[inner['mod_group']==g])
        if clean_inner.empty == False:
            clean_inner = clean_inner[['question', 'value', 'mod_index']]
            clean_inner.set_index('mod_index', inplace=True)
        else:
            return plain_subframe

        return final

    def compare_indices(self, plain_subframe, mod_subframe):

        if len(plain_subframe) != len(mod_subframe):
            maxLen = max(len(plain_subframe), len(mod_subframe))
            if len(list(plain_subframe.index)) > len(list(mod_subframe.index)):
                mod_filled_index = list(mod_subframe.index) + [None]*(maxLen - len(list(mod_subframe.index)))
                plain_filled_index = list(plain_subframe.index)
            else:
                plain_filled_index = list(plain_subframe.index) + [None]*(maxLen - len(list(plain_subframe.index)))
                mod_filled_index = list(mod_subframe.index)
        else:
            mod_filled_index = list(mod_subframe.index)
            plain_filled_index = list(plain_subframe.index)

        return plain_filled_index, mod_filled_index

    def write_names_and_titles(self, worksheet, workbook):
        # Note, center_across: True, does not format within the bounds of one cell!
        # Instead, it formats the text of the cell to take up as many cells across the width of the workbook as possible!
        # This behavior is dependant on adjacent cells also having the same format
        # ... bizarre

        # bold_cntrgrey = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'center_across': True, 'border': 1, 'top': 2, 'font_size': 12, 'text_wrap': True})
        # bold_cntrgrey.set_align('vcenter')
        bold_cntrgrey = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'border': 1, 'top': 2, 'font_size': 12, 'text_wrap': True})
        bold_cntrgrey.set_align('vcenter')
        bold_cntrgrey.set_align('center')
        # bold_cntrgrey_titles = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'center_across': True, 'border': 1, 'font_size': 12, 'text_wrap': True})
        # bold_cntrgrey_titles.set_align('vcenter')
        bold_cntrgrey_titles = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'border': 1, 'font_size': 12, 'text_wrap': True})
        bold_cntrgrey_titles.set_align('vcenter')
        bold_cntrgrey_titles.set_align('center')
        right_fmt_bold_cntrgrey = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'border': 1, 'font_size': 12, 'top': 2, 'right': 2, 'text_wrap': True})
        right_fmt_bold_cntrgrey.set_align('vcenter')
        right_fmt_bold_cntrgrey.set_align('center')
        # right_fmt_bold_cntrgrey_titles = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'center_across': True, 'border': 1, 'font_size': 12, 'right': 2, 'text_wrap': True})
        # right_fmt_bold_cntrgrey_titles.set_align('vcenter')
        right_fmt_bold_cntrgrey_titles = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'bg_color': '#e7e6e6', 'border': 1, 'font_size': 12, 'right': 2, 'text_wrap': True})
        right_fmt_bold_cntrgrey_titles.set_align('vcenter')
        right_fmt_bold_cntrgrey_titles.set_align('center')
        drk_gry_bar_hdr = workbook.add_format({'bg_color': '#808080', 'right': 2, 'text_wrap': True})


        if all(self.ppl_df.index==self.m_df.index) == False:
            logging.warning('The indices in the modified and plain files are mismatched.')
            # Output = [(x, y) for x, y in zip(self.ppl_df.index.tolist(), self.m_df.index.tolist()) if y != x]
            for x, y in zip(self.ppl_df.index.tolist(), self.m_df.index.tolist()):
                if x not in self.m_df.index.tolist():
                    print('\nPlain value: {}\nNot found in modified file'.format(x))
                if y not in self.ppl_df.index.tolist():
                    print('\nModified value: {}\nNot found in plain file'.format(y))

        # write names
        colNum = 1
        for i in self.m_df.index.tolist():
            if self.ppl_df.at[i, 'MiddleName'] is not None and self.ppl_df.at[i, 'MiddleName'] != '':
                name = self.ppl_df.at[i, 'FirstName'].strip() + ' ' + self.ppl_df.at[i, 'MiddleName'].strip()  + ' ' + self.ppl_df.at[i, 'LastName'].strip()
            else:
                name = self.ppl_df.at[i, 'FirstName'].strip() + ' ' + self.ppl_df.at[i, 'LastName'].strip()
            # print(name)
            if i == self.m_df.index.tolist()[-1]:
                # print(MiddleName)
                worksheet.write(0, colNum, name, right_fmt_bold_cntrgrey)
            else:
                worksheet.write(0, colNum, name, bold_cntrgrey)
            colNum += 1

        # write titles
        if 'Title' in self.ppl_df.columns:
            colNum = 1
            for i in self.m_df.index.tolist():
                value = self.ppl_df.loc[i]['Title']
                if i == self.m_df.index.tolist()[-1]:
                    worksheet.write(1, colNum, value, right_fmt_bold_cntrgrey_titles)
                else:
                    worksheet.write(1, colNum, value, bold_cntrgrey_titles)
                colNum += 1
        else:
            self.write_titles = False
        if self.write_titles == True:
            worksheet.merge_range(2, 1, 2, len(self.m_df.index.tolist()), ' ', drk_gry_bar_hdr)
        else:
            worksheet.merge_range(1, 1, 1, len(self.m_df.index.tolist()), ' ', drk_gry_bar_hdr)
        return

    def write_row_index(self, worksheet, workbook):
        indexfmt = workbook.add_format({"font_name": "Times New Roman", 'bg_color': '#fce4d6', 'border': 1, 'text_wrap': True, 'font_size': 12})
        indexfmt.set_align('left')
        indexfmt.set_align('top')
        indexfmt_right_text = workbook.add_format({"font_name": "Times New Roman", 'bg_color': '#fce4d6', 'border': 1, 'text_wrap': True, 'font_size': 12})
        indexfmt_right_text.set_align('right')
        salmon_bar_hdr = workbook.add_format({'align': 'center', "font_name": "Times New Roman", 'bold': 1, 'bg_color': '#fce4d6', 'right': 2, 'font_size': 14, 'top': 1, 'bottom': 1, 'left': 1, 'right': 2, 'text_wrap': True})
        salmon_bar_hdr.set_align('vcenter')

        # The default row number is 3. But if there are no title or if print_headers is false, then bump the row number back.
        # If there are headers without header symbology, modify the header symbol column so the first if condition below is satisfied
        row = 3
        if self.write_titles == False:
            row -= 1

        for question_id in self.questions_df.index:
            # This line will not produce headers for sections without header symbols.
            # Input files will need to denote sections without symbols using the header_symbol column
            if self.questions_df.loc[question_id]['header_symbol'] is not None and self.print_headers == True:
                worksheet.merge_range(row, 1, row, len(self.ppl_df['Capacity']), self.questions_df.loc[question_id]['header'], salmon_bar_hdr)
                row+=1
            elif self.questions_df.loc[question_id]['header_symbol'] == 'cert' and self.print_headers == False:
                worksheet.merge_range(row, 1, row, len(self.ppl_df['Capacity']), self.questions_df.loc[question_id]['header'], salmon_bar_hdr)
                row+=1

            # Formatted row index values should not include values that have been inserted as placeholders
            writeFlag = True

            if re.search('^blank_column_\d+$', self.questions_df.loc[question_id]['question_raw']) is not None:
                worksheet.write(row, 0, '', indexfmt)
                row+=1
                if self.mod_dict_of_dfs.get(question_id) is None and self.plain_dict_of_dfs.get(question_id) is None:
                    continue
                else:
                    writeFlag = False
                    pass

            # Handle Repeated Questions by removing the duplicate number
            elif re.search('^.*__\d+$', self.questions_df.loc[question_id]['question_raw']) is not None:
                frmttd_row = re.sub('__\d+$', '', self.questions_df.loc[question_id]['question_raw'])
                worksheet.write(row, 0, frmttd_row, indexfmt)
                row+=1
                if self.mod_dict_of_dfs.get(question_id) is None and self.plain_dict_of_dfs.get(question_id) is None:
                    continue
                else:
                    writeFlag = False
                    pass

            if self.mod_dict_of_dfs.get(question_id) is not None or self.plain_dict_of_dfs.get(question_id) is not None:
                try:
                    # print('\n', self.mod_dict_of_dfs.get(question_id))
                    if self.mod_dict_of_dfs.get(question_id).get('meta_data')[0] == 'boundMatrix':
                        print('\n\n-----------\nBound Type Matrix on row {}.\nResponse Data should always have a space after a semicolon'.format(row+1))
                    elif self.plain_dict_of_dfs.get(question_id).get('meta_data')[0] == 'boundMatrix':
                        print('\n\n-----------\nBound Type Matrix on row {}.\nResponse Data should always have a space after a semicolon'.format(row+1))
                except:
                    pass

                if writeFlag == True:
                    worksheet.write(row, 0, self.questions_df.loc[question_id]['question_raw'], indexfmt)
                    row+=1

                # Determine the length of the longest matrix question
                if self.mod_dict_of_dfs.get(question_id) is not None:
                    m_max = 0
                    key = self.mod_dict_of_dfs.get(question_id)
                    for respondent_id in self.mod_dict_of_dfs[question_id].keys():
                        if isinstance(self.mod_dict_of_dfs[question_id][respondent_id], pd.DataFrame):
                            if not self.mod_dict_of_dfs[question_id][respondent_id].empty:
                                if len(self.mod_dict_of_dfs[question_id][respondent_id]) > m_max:
                                    m_biggest_df = self.mod_dict_of_dfs[question_id][respondent_id]
                                    m_max = len(m_biggest_df)
                else:
                    m_biggest_df = None
                    m_max = None

                if self.plain_dict_of_dfs.get(question_id) is not None:
                    p_max = 0
                    key = self.plain_dict_of_dfs.get(question_id)
                    for respondent_id in self.plain_dict_of_dfs[question_id].keys():
                        if isinstance(self.plain_dict_of_dfs[question_id][respondent_id], pd.DataFrame):
                            if not self.plain_dict_of_dfs[question_id][respondent_id].empty:
                                if len(self.plain_dict_of_dfs[question_id][respondent_id]) > p_max:
                                    p_biggest_df = self.plain_dict_of_dfs[question_id][respondent_id]
                                    p_max = len(p_biggest_df)
                else:
                    p_biggest_df = None
                    p_max = None

                if p_max is not None and m_max is not None:
                    if p_max > m_max:
                        for q in p_biggest_df['question'].tolist():
                            worksheet.write(row, 0, q, indexfmt_right_text)
                            row+=1
                    else:
                        for q in m_biggest_df['question'].tolist():
                            worksheet.write(row, 0, q, indexfmt_right_text)
                            row+=1
                elif p_max is None and m_max is not None:
                    for q in m_biggest_df['question'].tolist():
                        worksheet.write(row, 0, q, indexfmt_right_text)
                        row+=1
                elif m_max is None and p_max is not None:
                    for q in p_biggest_df['question'].tolist():
                        worksheet.write(row, 0, q, indexfmt_right_text)
                        row+=1
                else:
                    logging.warning('This condition should never be satisfied')

            # If not a matrix question
            else:
                worksheet.write(row, 0, self.questions_df.loc[question_id]['question'], indexfmt)
                row+=1

    def get_opcodes(self, a, b):
        s = difflib.SequenceMatcher(None, a, b)
        arg_list = []
        for tag, i1, i2, j1, j2 in s.get_opcodes():
            # print('{:7}   a[{}:{}] --> b[{}:{}] {!r:>8} --> {!r}'.format(tag, i1, i2, j1, j2, a[i1:i2], b[j1:j2]))
            arg_list.append((tag, i1, i2, j1, j2, a[i1:i2], b[j1:j2]))
        return arg_list

    def get_diff_with_color(self, ppl_id, q_id, plain_value = '', mod_value = ''):
        fmt = self.workbook.add_format({"font_name": "Times New Roman", 'font_size': 12, 'border': 1, 'text_wrap': True})
        add = self.workbook.add_format({"font_name": "Times New Roman", 'font_size': 12, 'border': 1, 'underline': True, 'font_color': '#0000ff', 'text_wrap': True})
        sub = self.workbook.add_format({"font_name": "Times New Roman", 'font_size': 12, 'border': 1, 'font_color': '#ff0000', 'font_strikeout': 1, 'text_wrap': True})
        topleft = self.workbook.add_format({'align': 'left', 'valign': 'top', 'border': 1, 'text_wrap': True})
        fmt.set_align('left')
        fmt.set_valign('top')
        add.set_align('left')
        add.set_valign('top')
        sub.set_align('left')
        sub.set_valign('top')

        confirmationStatement = re.compile('[\s\t]*i[\s\t]+confirm[\s\t]+that[\s\t]+the', re.IGNORECASE)
        if confirmationStatement.match(mod_value):
            return [add, mod_value]

        try:
            if plain_value == '' and mod_value == '':
                return [fmt, '']
            elif plain_value == '' and mod_value != '':
                return [add, mod_value]
            elif plain_value != '' and mod_value == '':
                return [sub, plain_value]
            else:
                list_of_codes = self.get_opcodes(plain_value.split(), mod_value.split())
        except:
            print('plain_value: ',plain_value)
            print('mod_value: ', mod_value)

        list_of_fmts = []
        First = True
        for i in list_of_codes:
            tag, i1, i2, j1, j2, before, after = i[0], i[1], i[2], i[3], i[4], i[5], i[6]
            if First:
                if tag == 'equal':
                    list_of_fmts.extend([fmt, ' '.join(map(str, before))])
                elif tag == 'delete':
                    list_of_fmts.extend([sub, ' '.join(map(str, before))])
                elif tag == 'replace':
                    list_of_fmts.extend([sub, ' '.join(map(str, before))])
                    list_of_fmts.extend([fmt, ' '])
                    list_of_fmts.extend([add, ' '.join(map(str, after))])
                elif tag == 'insert':
                    list_of_fmts.extend([add, ' '.join(map(str, after))])
                else:
                    logging.warning('Warning! All op_code tags should be equal, delete, insert, or replace. Not:')
                    print(tag, len(tag), type(tag))
                First = False
            else:
                if tag == 'equal':
                    # list_of_fmts[-1] = ' ' + list_of_fmts[-1]
                    list_of_fmts.extend([fmt, ' '])
                    list_of_fmts.extend([fmt, ' '.join(map(str, before))])
                elif tag == 'delete':
                    # list_of_fmts[-1] = ' ' + list_of_fmts[-1]
                    list_of_fmts.extend([fmt, ' '])
                    list_of_fmts.extend([sub, ' '.join(map(str, before))])
                elif tag == 'replace':
                    # list_of_fmts[-1] = ' ' + list_of_fmts[-1]
                    list_of_fmts.extend([fmt, ' '])
                    list_of_fmts.extend([sub, ' '.join(map(str, before))])
                    # list_of_fmts[-1] = ' ' + list_of_fmts[-1]
                    list_of_fmts.extend([fmt, ' '])
                    list_of_fmts.extend([add, ' '.join(map(str, after))])
                elif tag == 'insert':
                    # list_of_fmts[-1] = ' ' + list_of_fmts[-1]
                    list_of_fmts.extend([fmt, ' '])
                    list_of_fmts.extend([add, ' '.join(map(str, after))])
                else:
                    logging.warning('Warning! All op_code tags should be equal, delete, insert, or replace. Not:')
                    print(tag, len(tag), type(tag))
        if len(list_of_fmts) > 2:
            list_of_fmts.extend([topleft])

        return list_of_fmts

    def write_data(self, worksheet, workbook, modified = False, plain = False):
        fmt = workbook.add_format({"font_name": "Times New Roman", 'font_size': 12, 'border': 1, 'text_wrap': True})
        last_col_fmt = workbook.add_format({"font_name": "Times New Roman", 'font_size': 12, 'top': 1, 'bottom': 1, 'left': 1, 'right': 2, 'text_wrap': True})
        bold_fmt = workbook.add_format({"font_name": "Times New Roman", 'bold': 1, 'text_wrap': True})
        blank_border = workbook.add_format({"font_name": "Times New Roman", 'border': 1, 'text_wrap': True})
        fmt.set_align('left')
        fmt.set_valign('top')
        col = 1
        for ppl_id in self.m_df.index:
            row = 3
            if self.write_titles == False:
                row -= 1
            for q_id in self.questions_df.index:
                if (self.plain_dict_of_dfs.get(q_id) is not None) or (self.mod_dict_of_dfs.get(q_id) is not None):
                    max_df_len = 0
                    if self.plain_dict_of_dfs.get(q_id) is not None:
                        for respondent_id in self.plain_dict_of_dfs[q_id].keys():
                            if self.plain_dict_of_dfs.get(q_id).get(respondent_id) is not None:
                                if isinstance(self.plain_dict_of_dfs[q_id][respondent_id], pd.DataFrame):
                                    if len(self.plain_dict_of_dfs[q_id][respondent_id]) > max_df_len:
                                        max_df_len = len(self.plain_dict_of_dfs[q_id][respondent_id])
                    if self.mod_dict_of_dfs.get(q_id) is not None:
                        for respondent_id in self.mod_dict_of_dfs[q_id].keys():
                            if self.mod_dict_of_dfs.get(q_id).get(respondent_id) is not None:
                                if isinstance(self.mod_dict_of_dfs[q_id][respondent_id], pd.DataFrame):
                                    if len(self.mod_dict_of_dfs[q_id][respondent_id]) > max_df_len:
                                        max_df_len = len(self.mod_dict_of_dfs[q_id][respondent_id])

                    # Automatically skip a row, and if the row begins a section, skip another row
                    if self.questions_df.loc[q_id]['header_symbol'] is not None and self.print_headers == True:
                        row += 1
                    elif self.questions_df.loc[q_id]['header_symbol'] =='cert' and self.print_headers == False:
                        row+=1
                    worksheet.write(row, col, '', blank_border)
                    row += 1

                    plain_dict_item = self.plain_dict_of_dfs.get(q_id)
                    if plain_dict_item is not None:
                        plain_subframe= plain_dict_item.get(ppl_id)
                    else:
                        plain_subframe = None

                    mod_dict_item = self.mod_dict_of_dfs.get(q_id)
                    if mod_dict_item is not None:
                        mod_subframe= mod_dict_item.get(ppl_id)
                    else:
                        mod_subframe = None
                    mod_subframe = mod_dict_item.get(ppl_id)

                    lastRow = row + max_df_len
                    if plain_subframe is None and mod_subframe is None:
                        while row < lastRow:
                            worksheet.write(row, col, '', blank_border)
                            row += 1
                        continue

                    elif plain_subframe is None and mod_subframe is not None:
                        # Fill out the plain_subframe if it's None
                        plain_subframe = pd.DataFrame(index=mod_subframe.index, columns=mod_subframe.columns)
                        plain_subframe.fillna('', inplace=True)

                        rowSkipCount = max_df_len - len(mod_subframe)

                        # Write row/column header questions
                        for p_index, m_index in zip(plain_subframe.index, mod_subframe.index):
                            p_value = plain_subframe.at[p_index, 'value']
                            m_value = mod_subframe.at[m_index, 'value']
                            list_of_fmts_and_values = self.get_diff_with_color(ppl_id, q_id, plain_value = p_value, mod_value = m_value)
                            if len(list_of_fmts_and_values)==0:
                                worksheet.write(row, col, '', blank_border)
                                row += 1
                            elif len(list_of_fmts_and_values) == 2:
                                worksheet.write(row, col, list_of_fmts_and_values[1], list_of_fmts_and_values[0])
                                row += 1
                            elif len(list_of_fmts_and_values) > 2:
                                worksheet.write_rich_string(row, col, *list_of_fmts_and_values)
                                row += 1
                            else:
                                logging.warning('get_diff_with_color did not return a list!')

                        while rowSkipCount > 0:
                            worksheet.write(row, col, '', blank_border)
                            rowSkipCount -= 1
                            row += 1

                    elif plain_subframe is not None and mod_subframe is None:
                        # Fill out the mod_subframe if it is None
                        mod_subframe = pd.DataFrame(index=plain_subframe.index, columns=plain_subframe.columns)
                        mod_subframe.fillna('', inplace=True)

                        rowSkipCount = max_df_len - len(plain_subframe)

                        # Write row/column header questions
                        for p_index, m_index in zip(plain_subframe.index, mod_subframe.index):
                            p_value = plain_subframe.at[p_index, 'value']
                            m_value = mod_subframe.at[m_index, 'value']
                            list_of_fmts_and_values = self.get_diff_with_color(ppl_id, q_id, plain_value = p_value, mod_value = m_value)
                            if len(list_of_fmts_and_values)==0:
                                worksheet.write(row, col, '', blank_border)
                                row += 1
                            elif len(list_of_fmts_and_values) == 2:
                                worksheet.write(row, col, list_of_fmts_and_values[1], list_of_fmts_and_values[0])
                                row += 1
                            elif len(list_of_fmts_and_values) > 2:
                                worksheet.write_rich_string(row, col, *list_of_fmts_and_values)
                                row += 1
                            else:
                                logging.warning('get_diff_with_color did not return a list!')

                        while rowSkipCount > 0:
                            worksheet.write(row, col, '', blank_border)
                            rowSkipCount -= 1
                            row += 1

                    # If both DataFrames exist
                    else:
                        # set up comparable indices. This takes into account different index sizes
                        # plain_subframe = self.align_groups_of_matrix_questions(plain_subframe, mod_subframe)
                        plain_filled_index, mod_filled_index = self.compare_indices(plain_subframe, mod_subframe)
                        # Get number of rows for this df and establish rowSkipCount

                        # plain filled index is always the same as mod_filled_index
                        rowSkipCount = max_df_len - len(plain_filled_index)

                        # Write row/column header questions
                        for p_index, m_index in zip(plain_filled_index, mod_filled_index):
                            if p_index is None:
                                p_value = ''
                            else:
                                p_value = plain_subframe.at[p_index, 'value']
                            if m_index is None:
                                m_value = ''
                            else:
                                m_value = mod_subframe.at[m_index, 'value']
                            if modified == False and plain == False:
                                list_of_fmts_and_values = self.get_diff_with_color(ppl_id, q_id, plain_value = p_value, mod_value = m_value)
                                if len(list_of_fmts_and_values)==0:
                                    worksheet.write(row, col, '', blank_border)
                                    row += 1
                                elif len(list_of_fmts_and_values) == 2:
                                    worksheet.write(row, col, list_of_fmts_and_values[1], list_of_fmts_and_values[0])
                                    row += 1
                                elif len(list_of_fmts_and_values) > 2:
                                    # print(list_of_fmts_and_values)
                                    worksheet.write_rich_string(row, col, *list_of_fmts_and_values)
                                    row += 1
                                else:
                                    logging.warning('get_diff_with_color did not return a list!')
                            elif modified == False and plain == True:
                                worksheet.write(row, col, p_value, fmt)
                                row += 1
                            elif modified == True and plain == False:
                                worksheet.write(row, col, m_value, fmt)
                                row += 1
                            else:
                                logging.warning('This else condition should never be satisfied')
                                row += 1

                        while rowSkipCount > 0:
                            worksheet.write(row, col, '', blank_border)
                            rowSkipCount -= 1
                            row += 1

                # If it is not a variable length question
                else:
                    if self.questions_df.loc[q_id]['header_symbol'] is not None and self.print_headers == True:
                        row += 1
                    elif self.questions_df.loc[q_id]['header_symbol'] =='cert' and self.print_headers == False:
                        row+=1
                    if modified == False and plain == False:
                        p_value = self.p_df.loc[ppl_id, self.p_df.columns[q_id]]
                        m_value = self.m_df.loc[ppl_id, self.m_df.columns[q_id]]
                        list_of_fmts_and_values = self.get_diff_with_color(ppl_id, q_id, plain_value = p_value, mod_value = m_value)

                        if len(list_of_fmts_and_values)==0:
                            worksheet.write(row, col, '', blank_border)
                            row += 1
                        elif len(list_of_fmts_and_values) == 2:
                            worksheet.write(row, col, list_of_fmts_and_values[1], list_of_fmts_and_values[0])
                            row += 1
                        elif len(list_of_fmts_and_values) > 2:
                            worksheet.write_rich_string(row, col, *list_of_fmts_and_values)
                            row += 1
                        else:
                            logging.warning('get_diff_with_color did not return a list!')
                            row += 1
                    elif modified == True and plain == False:
                        worksheet.write(row, col, self.m_df.loc[ppl_id, self.m_df.columns[q_id]], fmt)
                        row += 1
                    elif modified == False and plain == True:
                        worksheet.write(row, col, self.p_df.loc[ppl_id, self.p_df.columns[q_id]], fmt)
                        row += 1
                    else:
                        logging.warning('Specify either both modified and plain should be false, or only one can be true. Do not specify both arguments as True')
            col += 1

    def fsr_handler(self):
        # produce_plain = self.ask_user_plain_or_mod(type='plain')
        produce_plain = False
        produce_mod = self.ask_user_plain_or_mod(type='mod')

        worksheet = self.workbook.add_worksheet(self.file_name[0:31])
        if produce_plain:
            pworksheet = self.plain_workbook.add_worksheet(self.file_name[0:31])
        if produce_mod:
            mworksheet = self.mod_workbook.add_worksheet(self.file_name[0:31])

        self.write_names_and_titles(worksheet, self.workbook)
        if produce_plain:
            self.write_names_and_titles(pworksheet, self.plain_workbook)
        if produce_mod:
            self.write_names_and_titles(mworksheet, self.mod_workbook)

        self.write_row_index(worksheet, self.workbook)
        if produce_plain:
            self.write_row_index(pworksheet, self.plain_workbook)
        if produce_mod:
            self.write_row_index(mworksheet, self.mod_workbook)

        self.write_data(worksheet, self.workbook)
        if produce_plain:
            self.write_data(pworksheet, self.plain_workbook, plain=True)
        if produce_mod:
            self.write_data(mworksheet, self.mod_workbook, modified=True)

        worksheet.set_column(0, 0, 60)
        if produce_plain:
            pworksheet.set_column(0, 0, 60)
        if produce_mod:
            mworksheet.set_column(0, 0, 60)
        for c in range(1, len(self.m_df.index)+1):
            worksheet.set_column(c, c, 25)
            if produce_mod:
                mworksheet.set_column(c, c, 25)
        if produce_plain:
            for c in range(1, len(self.p_df.index)+1):
                pworksheet.set_column(c, c, 25)

        self.workbook.close()
        if produce_plain:
            self.plain_workbook.close()
        if produce_mod:
            self.mod_workbook.close()


'''
Many CSV readers implemented like in pandas' read_csv, for example,
produce have undesirable default behaviur. Reason being, those functions
don't expect repeated column headers, blank column headers, and other
characteristics often present in FSR input data. The opener function controls for
some of these characteristics
'''
def opener(file):
    import csv
    from collections import OrderedDict
    # with open('columns.csv', encoding="utf8", mode='r') as csv_file:
    #     csv_reader = csv.reader(csv_file, delimiter=',')
    #     line_count = 0
    #     rows = []
    #     for row in csv_reader:
    #         line_count += 1
    #         rows.append(row)
    #     print(f'Processed {line_count} lines.')
    #     return rows
    try:
        columns = []
        with open(file, encoding = "utf8", mode='r') as f:
            reader = csv.reader(f)
            for row in reader:
                if columns:
                    row = [re.sub(r'\xa0', ' ', i) for i in row]
                    row = [re.sub('%%', '', i) for i in row]
                    row = [re.sub('\ufeff', '', i) for i in row]
                    for i, value in enumerate(row):
                        columns[i].append(value)
                else:
                    row = [re.sub(r'\xa0', ' ', i) for i in row]
                    row = [re.sub('%%', '', i) for i in row]
                    row = [re.sub('\ufeff', '', i) for i in row]
                    # first row
                    blank_column_count = 1
                    duplicate_column_count = 1
                    duplicate_tracker = []
                    for i, value in enumerate(row):
                        if value == '':
                            emptyColName = 'blank_column_{}'.format(blank_column_count)
                            row[i] = emptyColName
                            blank_column_count += 1
                            duplicate_tracker.append(emptyColName)
                        elif value in duplicate_tracker:
                            row[i] = value + '__{}'.format(duplicate_column_count)
                            duplicate_column_count += 1
                            duplicate_tracker.append(row[i])
                        else:
                            duplicate_tracker.append(value)
                            pass
                    columns = [[value] for value in row]
        as_dict = OrderedDict((subl[0], subl[1:]) for subl in columns)
        # print(as_dict.keys())
        return as_dict
    except:
        columns = []
        with open(file, encoding = "ISO-8859-1", mode='r') as f:
            reader = csv.reader(f)
            for row in reader:
                if columns:
                    row = [re.sub(r'\xa0', ' ', i) for i in row]
                    row = [re.sub('%%', '', i) for i in row]
                    row = [re.sub('\ufeff', '', i) for i in row]
                    for i, value in enumerate(row):
                        columns[i].append(value)
                else:
                    row = [re.sub(r'\xa0', ' ', i) for i in row]
                    row = [re.sub('%%', '', i) for i in row]
                    row = [re.sub('\ufeff', '', i) for i in row]
                    # first row
                    blank_column_count = 1
                    duplicate_column_count = 1
                    duplicate_tracker = []
                    for i, value in enumerate(row):
                        if value == '':
                            emptyColName = 'blank_column_{}'.format(blank_column_count)
                            row[i] = emptyColName
                            blank_column_count += 1
                            duplicate_tracker.append(emptyColName)
                        elif value in duplicate_tracker:
                            row[i] = value + '__{}'.format(duplicate_column_count)
                            duplicate_column_count += 1
                            duplicate_tracker.append(row[i])
                        else:
                            duplicate_tracker.append(value)
                            pass
                    columns = [[value] for value in row]

        as_dict = OrderedDict((subl[0], subl[1:]) for subl in columns)
    return as_dict

'''
The FSR class could easily produce this information within itself, but state CBE
pipeline is such that I wanted to mimic the input to the FSR class such that
it approaches something that is forward compatible with future database integration
'''
def ppl_parser(df):
    # Create dataframe of respondents
    ppl = dict()
    cols = ['UniqueIdentifier', 'FirstName', 'LastName', 'MiddleName', 'Capacity', 'Audit', 'Compensation', 'NomGov', 'HR', 'Risk', 'Trust', 'hsse', 'Regulatory', 'Compliance', 'Corporate', 'Title']
    for k, v in df.items():
        if k in cols:
            ppl[k] = v

    ppl_df = pd.DataFrame.from_dict(ppl)
    if  'UniqueIdentifier' in ppl_df.columns:
        ppl_df.set_index('UniqueIdentifier', inplace=True)

    if None in set(ppl_df['Capacity'].tolist()) or '' in set(ppl_df['Capacity'].tolist()):
        logging.warning('Null values in Capacity column.\nppl_df will not be sorted.')
        return ppl_df

    orderedCapacities = ['Both', 'Director', 'Officer']
    df_list = []
    for i in orderedCapacities:
        d = ppl_df[ppl_df['Capacity']==i]
        d.sort_values(['LastName', 'FirstName'], inplace=True)
        df_list.append(d)
    ppl_df = pd.concat(df_list)

    return ppl_df

'''
Convert Roman numerals to arabic
'''
def to_arabic(self, roman):
    roman = self.check_valid(roman)
    keys = ['IV', 'IX', 'XL', 'XC', 'CD', 'CM', 'I', 'V', 'X', 'L', 'C', 'D', 'M']
    to_arabic = {'IV': '4', 'IX': '9', 'XL': '40', 'XC': '90', 'CD': '400', 'CM': '900',
            'I': '1', 'V': '5', 'X': '10', 'L': '50', 'C': '100', 'D': '500', 'M': '1000'}
    for key in keys:
        if key in roman:
            roman = roman.replace(key, ' {}'.format(to_arabic.get(key)))
    arabic = sum(int(num) for num in roman.split())
    return arabic

def check_valid(self, roman):
    roman = roman.upper()
    invalid = ['IIII', 'VV', 'XXXX', 'LL', 'CCCC', 'DD', 'MMMM']
    if any(sub in roman for sub in invalid):
        raise ValueError('Numerus invalidus est: {}\n.The number is an invalid Roman Numeral'.format(roman))
    return roman

def ask_user():
    check = str(input("Would you like section headers? (y/n): ")).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return ask_user()
    except Exception as error:
        print("Please enter valid inputs")
        print(error)
        return ask_user()

'''
parse_raw_fsr performs basic data parsing.
'''
def parse_raw_fsr(dictionary):
    raw = pd.DataFrame.from_dict(dictionary)

    # The line below serves to indicate where there might be "blank" rows or
    # columns in the raw csv. Users can then delete these extraneous rows or
    # columns to align the data.
    print('\nRaw Shape: ', raw.shape)

    if  'UniqueIdentifier' in raw.columns:
        raw.set_index('UniqueIdentifier', inplace=True)
    columns = raw.columns

    # Identify where the data begins and ends.
    # Identify what kind of survey style is in use
    re_alpha_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(A)[\s\t]?[\.:—-]')
    alpha_firstCols = list(filter(re_alpha_first_col.match, columns))
    re_roman_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(I)[\s\t]?[\.:—-]')
    roman_firstCols = list(filter(re_roman_first_col.match, columns))
    re_arabic_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(1)[\s\t]?[\.:—-]')
    arabic_firstCols = list(filter(re_arabic_first_col.match, columns))
    # Some reports have data that proceed the first instance of section symbology
    # Example: GENERAL INFORMATION
    # To present
    re_multipleWords_first_col = re.compile('\w+[\s\t]+\w+')
    multipleWords = list(filter(re_multipleWords_first_col.match, columns))
    re_one_word_ends_with_colon = re.compile('\w+:')
    oneWordColon = list(filter(re_one_word_ends_with_colon.match, columns))
    # Sometimes more than one style will be found
    first_col_list = []
    symbols = []
    if alpha_firstCols:
        alpha_firstCol = alpha_firstCols[0]
        alpha_symbol = re_alpha_first_col.match(alpha_firstCol).group(2)
        first_col_list.append(alpha_firstCol)
        symbols.append(alpha_symbol)
    if roman_firstCols:
        roman_firstCol = roman_firstCols[0]
        roman_symbol = re_roman_first_col.match(roman_firstCol).group(2)
        first_col_list.append(roman_firstCol)
        symbols.append(roman_symbol)
    if arabic_firstCols:
        arabic_firstCol = arabic_firstCols[0]
        arabic_symbol = re_arabic_first_col.match(arabic_firstCol).group(2)
        first_col_list.append(arabic_firstCol)
        symbols.append(arabic_symbol)
    if multipleWords:
        multipleWords_first_col = multipleWords[0]
        first_col_list.append(multipleWords_first_col)
        symbols.append('')
    if oneWordColon:
        oneWordFirstColumn = oneWordColon[0]
        first_col_list.append(oneWordFirstColumn)
        symbols.append('w')

    if len(symbols)==0:
        logging.warning('No symbology found!')

    # It's important to note here that, above, multipleWords is tested for last.
    # Because multipleWords is appended to first_col_list last, the first item in
    # first_col_list shouldn't be ''.

    # If there are reports using no symbology style, then the loop below will
    # need to be changed, and the last condition below resulting in:
    # logging.warning('\nNO SYMBOL!\n')
    # will need to be swapped out for handling of word column symobology
    for fc, sym in zip(first_col_list, symbols):
        if fc == first_col_list[0]:
            first_col_number = raw.columns.get_loc(fc)
            first_col_name = fc
            if sym != '' and sym != 'w':
                symbol = sym
        else:
            if raw.columns.get_loc(fc) < first_col_number:
                first_col_number = raw.columns.get_loc(fc)
                first_col_name = fc
                if sym != '' and sym != 'w':
                    symbol = sym
            else:
                continue


    # Trim down to the raw data
    data = raw.iloc[:, first_col_number:]
    questions_df = pd.DataFrame({'question_raw': data.columns})
    header_symbol_list = []
    questions = []
    header_list = []
    re_scrubbed_continued = re.compile('.*\(continued\)[\t\s]*(.*)', re.IGNORECASE)
    # re_headers only works in conjunction with each version of re_sections
    re_headers = re.compile('(.*?)\s\s')
    re_no_space_header_for_arabic = re.compile('(.*?\d+[\.:]?)')
    # re_test = re.compile('(.*?\d+[\s\t]?[\.:—–][\s\t]?.*?)(\s\s|(?<!:))')

    # Do the work of identifying
    for i, value in zip(questions_df.index, questions_df['question_raw']):
        if symbol == 'A':
            # section_name_with_number_and_content_between = re.compile('(?=(^|^[\t\s]*|^Part[\s\t]+)(^[A-Z]\.\s+[/A-Z0-9\(\)"\'\s\t\.-]+)[\t\s]*(_<)?[A-Z]?[a-z]+.*1\.\s+.*')
            re_sections = re.compile('^(?!.*\(Continued\))[\t\s]*(Part[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([A-Z]{1,2})[\s\t]?[\.:—–][\s\t]?(.*)')
            section_groups = re_sections.match(value)
            # print('\nvalue:', value)
            scrubbed_continued = re_scrubbed_continued.match(value)
            if section_groups is not None:
                # print('\nvalue:', value)
                # print(section_groups.groups())
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                    header = re_headers.match(value)
                    if header is not None:
                        header_list.append(header.group(1))
                    else:
                        logging.warning('No formatted header found')
                        header_list.append(None)
                else:
                    header_symbol_list.append(None)
                    header_list.append(None)
                # print('\nvalue:', value)
                # print(section_groups.groups())
                questions.append(section_groups.group(4))
            elif scrubbed_continued is not None:
                # print('\n')
                # print(scrubbed_continued.group(0))
                # print(scrubbed_continued.group(1))
                questions.append(scrubbed_continued.group(1))
                header_symbol_list.append(None)
                header_list.append(None)
            else:
                header_symbol_list.append(None)
                questions.append(value)
                header_list.append(None)

        elif symbol == 'I':
            re_sections = re.compile('^(?!.*\((?i:continued)\))[\t\s]*((?i:part)[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([IVX]+)[\s\t]?[\.:—–][\s\t]?(.*)')
            section_groups = re_sections.match(value)
            scrubbed_continued = re_scrubbed_continued.match(value)
            if section_groups is not None:
                # print('\nvalue:', value)
                # print(section_groups.groups())
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                    header = re_headers.match(value)
                    if header is not None:
                        header_list.append(header.group(1))
                    else:
                        logging.warning('No formatted header found')
                        header_list.append(None)
                else:
                    header_symbol_list.append(None)
                    header_list.append(None)
                questions.append(section_groups.group(4))
            elif scrubbed_continued is not None:
                # print('scrubbed_continued is not None')
                # print(scrubbed_continued.group(0))
                # print(scrubbed_continued.group(1))
                questions.append(scrubbed_continued.group(1))
                header_symbol_list.append(None)
                header_list.append(None)
            else:
                header_symbol_list.append(None)
                questions.append(value)
                header_list.append(None)

        elif symbol == '1':
            re_sections = re.compile('^(?!.*\(Continued\))[\t\s]*(Part[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([\d]+)[\s\t]?[\.:—–][\s\t]?(?!\d)(.*)')
            section_groups = re_sections.match(value)
            scrubbed_continued = re_scrubbed_continued.match(value)
            if section_groups is not None:
                # print('\nvalue:', value)
                # print(section_groups.groups())
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                    header = re_headers.match(value)
                    # header = re_test.match(value)
                    no_space_header = re_no_space_header_for_arabic.match(value)
                    if header is not None:
                        header_list.append(header.group(1))
                    elif no_space_header is not None:
                        header_list.append(no_space_header.group(1))
                    else:
                        header_list.append(None)
                else:
                    header_symbol_list.append(None)
                    header_list.append(None)
                # print('\nvalue:', value)
                # print(section_groups.groups())
                questions.append(section_groups.group(4))
            elif scrubbed_continued is not None:
                # print('\n')
                # print(scrubbed_continued.group(0))
                # print(scrubbed_continued.group(1))
                questions.append(scrubbed_continued.group(1))
                header_symbol_list.append(None)
                header_list.append(None)
            else:
                header_symbol_list.append(None)
                questions.append(value)
                header_list.append(None)

        else:
            logging.warning('\nNO SYMBOL STYLE FOUND!\n')

    questions_df['header_symbol'] = header_symbol_list
    questions_df['question'] = questions
    questions_df['header'] = header_list


    cert_col = re.compile('(?!.*blank_column_)(?=.*(CERTIFICATION|ACKNOWLEDGEMENT|ACKNOWLEDGE))(?!.*__\d+$)(.*?)_(.*)_([\w]+)?', re.IGNORECASE)
    cert_section_list = []
    for i, raw_question in zip(questions_df.index, questions_df['question_raw']):
        cert_match = cert_col.match(raw_question)
        if cert_match is not None and cert_match.group(2) not in cert_section_list:
            # print('\ngroup2',cert_match.group(2))
            cert_section_list.append(cert_match.group(2))

            questions_df.at[i, 'header_symbol'] = 'cert'
            questions_df.at[i, 'header'] = cert_match.group(2)
            questions_df.at[i, 'question'] = cert_match.group(3)

            # print('\nstart header')
            # print(cert_match.groups())
        elif cert_match is not None:
            questions_df.at[i, 'question'] = cert_match.group(3)
            # print('\nnot beginning header')
            # print(cert_match.groups())
        else:
            pass

    # Table question data
    dict_of_dfs = {}

    # print('SHAPE', data.shape, data.columns)

    # Check that the last instance of row/column data is retrieved
    ##########################
    # matrix comp re
    # re_row_multiValued = re.compile('(.*?)/RowHeader(\d+):+(.*?);(?=[^;]*?(/RowHeader)\d+|$)')
    ##########################
    re_detectsEverythingExceptForResponseCasewsemiw = re.compile('(.*?[^\s\t]/[^\s\t].*?:):*(.*?);(?=([^\s\t]|$))')
    re_row_multiValued = re.compile('(.*?)(/(RowHeader)\d+:+)(.*?);(?=[^;]*?(/RowHeader)\d+|$)')
    re_col_multiValued = re.compile('ColumnHeader\d+/(.*?:):?(?=([^:]*?)(;ColumnHeader\d+/|;$))')
    # re_unlabeled_multiValued_original = re.compile('(.*?/.*?):+(.*?);')
    # re_unlabeled_multiValued_usefulInDetectingListsOfRelativeInformation = re.compile('([^\s]*?/[^\s]*?):+(.*?);(?=[^\s;]*?/[^\s]*?:+|$)')
    type_flag = ''
    for q, i in zip(questions_df['question_raw'], questions_df.index):
        if (data[q].apply(lambda x: bool(re_row_multiValued.match(x))).any()) | (data[q].apply(lambda x: bool(re_col_multiValued.match(x))).any()) | (data[q].apply(lambda x: bool(re_detectsEverythingExceptForResponseCasewsemiw.match(x))).any()):
            # print(q, i)
            question_dict = {}
            # Iterate through the identified column's rows
            for ind, row in zip(data.index, data[q]):
                # print(ind, row=='')
                if re_row_multiValued.match(row) is not None:
                    multi = re_row_multiValued.findall(row)
                    type_flag = 'row'
                elif re_col_multiValued.match(row) is not None:
                    multi = re_col_multiValued.findall(row)
                    type_flag = 'col'
                elif re_detectsEverythingExceptForResponseCasewsemiw.match(row) is not None:
                    multi = re_detectsEverythingExceptForResponseCasewsemiw.findall(row)
                    type_flag = 'boundMatrix'
                elif row=='':
                    continue
                else:
                    print(row)
                    logging.warning('\n\n--------\nERROR\nCould not distinguish between col and row multiheaders')

                # For each respondent that has matrix responses, populate a dataframe
                # that contains questions, responses, and 'group' values.
                # A group value refers to a grouping of questions. Groups of questions
                # often repeat. We will want to examine these groupings, so we'll label
                # them here.
                ###########################
                # Matrix comp df
                # individual_df = pd.DataFrame(columns=['question', 'value', 'group'])
                ###########################
                individual_df = pd.DataFrame(columns=['question', 'value'])
                for ii, m in enumerate(multi):
                    if type_flag == 'row':
                        individual_df.loc[ii] = [m[0], m[3]]
                    elif type_flag == 'col':
                        individual_df.loc[ii] = [m[0], m[1]]
                    elif type_flag == 'boundMatrix':
                        individual_df.loc[ii] = [m[0], m[1]]
                    else:
                        print('ERROR')
                nbr_of_unique_questions = individual_df['question'].nunique()
                if type_flag == 'row':
                    if (individual_df['question'].nunique() != len(individual_df['question'])) and (set(individual_df.iloc[-nbr_of_unique_questions:]['value'].tolist()).issubset([None, ''])):
                        # print('\n', ind)
                        # print(individual_df['question'].nunique() != len(individual_df['question']))
                        # print(individual_df['question'].nunique())
                        # print(individual_df)
                        while (set(individual_df.iloc[-individual_df['question'].nunique():]['value'].tolist()).issubset(['', None])) and (not individual_df.empty):
                            individual_df.drop(individual_df.tail(individual_df['question'].nunique()).index,inplace=True)
                elif type_flag == 'col':
                    # print(individual_df)
                    # individual_df.drop(individual_df.tail(individual_df['question'].nunique()).index,inplace=True)
                    # print(individual_df['value'].tolist()[-individual_df['question'].nunique():])
                    # if individual_df['question'].tolist()[-individual_df['question'].nunique():] == None:
                        # print('here!')
                    pass
                elif type_flag == 'boundMatrix':
                    pass
                else:
                    print('ERROR')
                question_dict[ind] = individual_df
            if type_flag == 'row':
                question_dict['meta_data'] = ['rowTypeData']
            elif type_flag == 'col':
                question_dict['meta_data'] = ['columnTypeData']
            elif type_flag == 'boundMatrix':
                question_dict['meta_data'] = ['boundMatrix']
            else:
                logging.warning('type_flag error')
            dict_of_dfs[i] = question_dict

    return data, questions_df, dict_of_dfs, symbol

if __name__ == "__main__":
    start_time = time.time()
    print(sys.version)
    print('\n------------------------------------------\n')

    cwd = os.getcwd()
    print(cwd)
    plain = re.compile('(?!^~).*plain(.*)\.csv$', re.IGNORECASE)
    modified = re.compile('(?!^~).*modified(.*)\.csv$', re.IGNORECASE)

    for file in os.listdir(cwd):
        if modified.match(file):
            print('\n-----------------\nProccessing Modified...')
            print('Modified File Name: ', file)
            mdf_raw = opener(file)
            mdf, mquestions_df, mdict_of_dfs, symbol  = parse_raw_fsr(mdf_raw)
    for file in os.listdir(cwd):
        if plain.match(file):
            print('\n\n\n-----------------\nProcessing Plain...')
            print('Plain File Name: ', file)
            pdf_raw = opener(file)
            pdf, pquestions_df, pdict_of_dfs, symbol = parse_raw_fsr(pdf_raw)
    print('\n')
    ppl_df = ppl_parser(pdf_raw)
    if symbol == '1':
        print_headers = ask_user()
    else:
        print_headers = True

    ####################################
    # print(mdict_of_dfs)
    #####################################
    # writer = pd.ExcelWriter('ppl.xlsx')
    # ppl_df.to_excel(writer, 'ppl_df')
    # writer.save()
    # writer = pd.ExcelWriter('mdf.xlsx')
    # mdf.to_excel(writer, 'mdf')
    # writer.save()
    # writer = pd.ExcelWriter('pdf.xlsx')
    # pdf.to_excel(writer, 'pdf')
    # writer.save()
    # writer = pd.ExcelWriter('questions.xlsx')
    # pquestions_df.to_excel(writer, 'plain questions')
    # writer.save()
    #####################################

    if mquestions_df.equals(pquestions_df):
        print('Do the two input files have equivalent column values: ', mquestions_df.equals(pquestions_df), '\n')
    else:
        # print(pd.concat([mquestions_df,pquestions_df]).drop_duplicates(keep=False))
        # print(list(set(list(mquestions_df.index)) - set(list(pquestions_df.index))))
        logging.warning('The two input files have differing column values!')

    # If the plain file has no matrix data, fill out the dictionary for ease in matching
    if not pdict_of_dfs and mdict_of_dfs:
        for k, v in mdict_of_dfs.items():
            pdict_of_dfs[k] = {}
            for k1, v1 in v.items():
                pdict_of_dfs[k][k1] = pd.DataFrame()

    # print(mquestions_df)
    FSR(pdf, mdf, ppl_df, mquestions_df, pdict_of_dfs, mdict_of_dfs, print_headers)

    #####################################
    # print(ppl_df)
    # # print(list(set(list(pdf_raw.columns)) - set(list(mdf_raw.columns))))

    # pdf_raw.set_index('FullName', inplace=True)
    # mdf_raw.set_index('FullName', inplace=True)
    # # print('bool test', mdf_raw.index.tolist() == pdf_raw.index.tolist())
    #####################################
    print('\n\n--- Run Time ---')
    print('--- %s seconds ---' % (time.time() - start_time))
