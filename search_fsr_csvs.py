import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import openpyxl
import sys
import itertools
import difflib
from pprint import pprint
import logging
import time

def parse_raw_fsr(dictionary, results_worksheet, file_name):
    raw = pd.DataFrame.from_dict(dictionary)
    # print('\nRaw Shape: ', raw.shape)

    # if (list(raw.columns)[0] == '\ufeffUniqueIdentifier') | ((list(raw.columns)[0] == '\ufeffResponseID')):
    #     newFirstCol = re.sub('\ufeff', '', list(raw.columns)[0])
    #     raw = raw.rename(columns={list(raw.columns)[0]: newFirstCol})

    # if 'ResponseID' in raw.columns:
    #     raw.set_index('ResponseID', inplace=True)
    # elif  'UniqueIdentifier' in raw.columns:
    #     raw.set_index('UniqueIdentifier', inplace=True)
    if  'UniqueIdentifier' in raw.columns:
        raw.set_index('UniqueIdentifier', inplace=True)
    columns = raw.columns

    # Identify where the data begins and ends.
    # Identify what kind of survey style is in use
    re_alpha_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(A)[\s\t]?[\.:—-]')
    alpha_firstCols = list(filter(re_alpha_first_col.match, columns))
    re_roman_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(I)[\s\t]?[\.:—-]')
    roman_firstCols = list(filter(re_roman_first_col.match, columns))
    re_arabic_first_col = re.compile('^(?!.*\(Continued\))[A-Za-z\s\t]*(Part)?[\t\s]*(1)[\s\t]?[\.:—-]')
    arabic_firstCols = list(filter(re_arabic_first_col.match, columns))
    # Some reports have data that proceed the first instance of section symbology
    # Example: GENERAL INFORMATION
    # To present
    re_multipleWords_first_col = re.compile('\w+[\s\t]+\w+')
    multipleWords = list(filter(re_multipleWords_first_col.match, columns))
    re_one_word_ends_with_colon = re.compile('\w+:')
    oneWordColon = list(filter(re_one_word_ends_with_colon.match, columns))
    # Sometimes more than one style will be found
    first_col_list = []
    symbols = []
    if alpha_firstCols:
        alpha_firstCol = alpha_firstCols[0]
        alpha_symbol = re_alpha_first_col.match(alpha_firstCol).group(2)
        first_col_list.append(alpha_firstCol)
        symbols.append(alpha_symbol)
    if roman_firstCols:
        roman_firstCol = roman_firstCols[0]
        roman_symbol = re_roman_first_col.match(roman_firstCol).group(2)
        first_col_list.append(roman_firstCol)
        symbols.append(roman_symbol)
    if arabic_firstCols:
        arabic_firstCol = arabic_firstCols[0]
        arabic_symbol = re_arabic_first_col.match(arabic_firstCol).group(2)
        first_col_list.append(arabic_firstCol)
        symbols.append(arabic_symbol)
    if multipleWords:
        multipleWords_first_col = multipleWords[0]
        first_col_list.append(multipleWords_first_col)
        symbols.append('')
    if oneWordColon:
        oneWordFirstColumn = oneWordColon[0]
        first_col_list.append(oneWordFirstColumn)
        symbols.append('w')

    if len(symbols)==0:
        logging.warning('No symbology found!')

    for fc, sym in zip(first_col_list, symbols):
        if fc == first_col_list[0]:
            first_col_number = raw.columns.get_loc(fc)
            first_col_name = fc
            if sym != '' and sym != 'w':
                symbol = sym
        else:
            if raw.columns.get_loc(fc) < first_col_number:
                first_col_number = raw.columns.get_loc(fc)
                first_col_name = fc
                if sym != '' and sym != 'w':
                    symbol = sym
            else:
                continue

    # Trim down to the raw data
    data = raw.iloc[:, first_col_number:]
    questions_df = pd.DataFrame({'question_raw': data.columns})
    header_symbol_list = []
    questions = []
    header_list = []
    re_scrubbed_continued = re.compile('.*\(continued\)[\t\s]*(.*)', re.IGNORECASE)
    # re_headers only works in conjunction with each version of re_sections
    re_headers = re.compile('(.*?)\s\s')
    re_no_space_header_for_arabic = re.compile('(.*?\d+[\.:]?)')
    # re_test = re.compile('(.*?\d+[\s\t]?[\.:—–][\s\t]?.*?)(\s\s|(?<!:))')

    # Do the work of identifying
    for i, value in zip(questions_df.index, questions_df['question_raw']):
        if symbol == 'A':
            # section_name_with_number_and_content_between = re.compile('(?=(^|^[\t\s]*|^Part[\s\t]+)(^[A-Z]\.\s+[/A-Z0-9\(\)"\'\s\t\.-]+)[\t\s]*(_<)?[A-Z]?[a-z]+.*1\.\s+.*')
            re_sections = re.compile('^(?!.*\(Continued\))[\t\s]*(Part[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([A-Z]{1,2})[\s\t]?[\.:—–][\s\t]?(.*)')
            section_groups = re_sections.match(value)
            # print('\nvalue:', value)
            scrubbed_continued = re_scrubbed_continued.match(value)
            if section_groups is not None:
                # print('\nvalue:', value)
                # print(section_groups.groups())
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                    header = re_headers.match(value)
                    if header is not None:
                        header_list.append(header.group(1))
                    else:
                        logging.warning('No formatted header found')
                        header_list.append(None)
                else:
                    header_symbol_list.append(None)
                    header_list.append(None)
                # print('\nvalue:', value)
                # print(section_groups.groups())
                questions.append(section_groups.group(4))
            elif scrubbed_continued is not None:
                # print('\n')
                # print(scrubbed_continued.group(0))
                # print(scrubbed_continued.group(1))
                questions.append(scrubbed_continued.group(1))
                header_symbol_list.append(None)
                header_list.append(None)
            else:
                header_symbol_list.append(None)
                questions.append(value)
                header_list.append(None)

        elif symbol == 'I':
            # re_sections = re.compile('^(?!.*\((?i:continued)\))[\t\s]*((?i:part)[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([IVX]+)[\s\t]?[\.:—-](.*)')
            re_sections = re.compile('^(?!.*\((?i:continued)\))[\t\s]*((?i:part)[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([IVX]+)[\s\t]?[\.:—–][\s\t]?(.*)')
            section_groups = re_sections.match(value)
            scrubbed_continued = re_scrubbed_continued.match(value)
            if section_groups is not None:
                # print('\nvalue:', value)
                # print(section_groups.groups())
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                    header = re_headers.match(value)
                    if header is not None:
                        header_list.append(header.group(1))
                    else:
                        logging.warning('No formatted header found')
                        header_list.append(None)
                else:
                    header_symbol_list.append(None)
                    header_list.append(None)
                questions.append(section_groups.group(4))
            elif scrubbed_continued is not None:
                # print('scrubbed_continued is not None')
                # print(scrubbed_continued.group(0))
                # print(scrubbed_continued.group(1))
                questions.append(scrubbed_continued.group(1))
                header_symbol_list.append(None)
                header_list.append(None)
            else:
                header_symbol_list.append(None)
                questions.append(value)
                header_list.append(None)

        elif symbol == '1':
            re_sections = re.compile('^(?!.*\(Continued\))[\t\s]*(Part[\s\t]+)?([\w\t\s\[\]\(\)]+[\s\t]+)?([\d]+)[\s\t]?[\.:—–][\s\t]?(?!\d)(.*)')
            section_groups = re_sections.match(value)
            scrubbed_continued = re_scrubbed_continued.match(value)
            if section_groups is not None:
                # print('\nvalue:', value)
                # print(section_groups.groups())
                if section_groups.group(3) not in set(header_symbol_list):
                    header_symbol_list.append(section_groups.group(3))
                    header = re_headers.match(value)
                    # header = re_test.match(value)
                    no_space_header = re_no_space_header_for_arabic.match(value)
                    if header is not None:
                        header_list.append(header.group(1))
                    elif no_space_header is not None:
                        header_list.append(no_space_header.group(1))
                    else:
                        # logging.warning('No formatted header found!\nThere may be a formatted header in the question:\n')
                        # print('\nNo formatted header found!\nThere may be a formatted header in the question:')
                        # print(value)
                        header_list.append(None)
                else:
                    header_symbol_list.append(None)
                    header_list.append(None)
                # print('\nvalue:', value)
                # print(section_groups.groups())
                questions.append(section_groups.group(4))
            elif scrubbed_continued is not None:
                # print('\n')
                # print(scrubbed_continued.group(0))
                # print(scrubbed_continued.group(1))
                questions.append(scrubbed_continued.group(1))
                header_symbol_list.append(None)
                header_list.append(None)
            else:
                header_symbol_list.append(None)
                questions.append(value)
                header_list.append(None)

        else:
            logging.warning('\nNO SYMBOL STYLE FOUND!\n')

    questions_df['header_symbol'] = header_symbol_list
    questions_df['question'] = questions
    questions_df['header'] = header_list


    cert_col = re.compile('(?!.*blank_column_)(?=.*(CERTIFICATION|ACKNOWLEDGEMENT|ACKNOWLEDGE))(?!.*__\d+$)(.*?)_(.*)_([\w]+)?', re.IGNORECASE)
    cert_section_list = []
    for i, raw_question in zip(questions_df.index, questions_df['question_raw']):
        cert_match = cert_col.match(raw_question)
        if cert_match is not None and cert_match.group(2) not in cert_section_list:
            # print('\ngroup2',cert_match.group(2))
            cert_section_list.append(cert_match.group(2))

            questions_df.at[i, 'header_symbol'] = 'cert'
            questions_df.at[i, 'header'] = cert_match.group(2)
            questions_df.at[i, 'question'] = cert_match.group(3)

            # print('\nstart header')
            # print(cert_match.groups())
        elif cert_match is not None:
            questions_df.at[i, 'question'] = cert_match.group(3)
            # print('\nnot beginning header')
            # print(cert_match.groups())
        else:
            pass

    dict_of_dfs = {}

    re_multiValuedSearch = re.compile('(.*?[^\s\t]/[^\s\t].*?:+)(.*?);(?=([^\s\t]|$))')
    re_row_multiValued = re.compile('(.*?)(/(RowHeader)\d+:+)(.*?);(?=[^;]*?(/RowHeader)\d+|$)')
    re_col_multiValued = re.compile('ColumnHeader\d+/(.*?:):?(?=([^:]*?)(;ColumnHeader\d+/|;$))')
    re_unlabeled_multiValued = re.compile('([^\s]*?/[^\s]*?):+(.*?);(?=[^\s;]*?/[^\s]*?:+|$)')
    re_wildcard = re.compile('.*/.*:+.*;')
    for q, i in zip(questions_df['question_raw'], questions_df.index):
        # if (data[q].apply(lambda x: bool(re_row_multiValued.match(x))).any()) | (data[q].apply(lambda x: bool(re_col_multiValued.match(x))).any()) | (data[q].apply(lambda x: bool(re_unlabeled_multiValued.match(x))).any()):
        if (data[q].apply(lambda x: bool(re_multiValuedSearch.match(x))).any()) | (data[q].apply(lambda x: bool(re_unlabeled_multiValued.match(x))).any()) | (data[q].apply(lambda x: bool(re_wildcard.match(x))).any()) and not((data[q].apply(lambda x: bool(re_row_multiValued.match(x))).any()) | (data[q].apply(lambda x: bool(re_col_multiValued.match(x))).any())):
            new_column = results_worksheet.max_column +1
            results_worksheet.cell(row=1, column=new_column, value=file_name)
            results_worksheet.cell(row=2, column=new_column, value=q)
            if data[q].apply(lambda x: bool(re_multiValuedSearch.match(x))).any():
                results_worksheet.cell(row=4, column=new_column, value='+')
            # if data[q].apply(lambda x: bool(re_row_multiValued.match(x))).any():
            #     results_worksheet.cell(row=5, column=new_column, value='+')
            # if data[q].apply(lambda x: bool(re_col_multiValued.match(x))).any():
            #     results_worksheet.cell(row=6, column=new_column, value='+')
            if data[q].apply(lambda x: bool(re_unlabeled_multiValued.match(x))).any():
                results_worksheet.cell(row=7, column=new_column, value='+')
            if data[q].apply(lambda x: bool(re_wildcard.match(x))).any():
                results_worksheet.cell(row=8, column=new_column, value='+')

            for rowy, value in enumerate(data[q], start=10):
                results_worksheet.cell(row=rowy, column=new_column, value=value)

            wb = openpyxl.Workbook()
            ws = wb.active
            # if re.search('.*honeywell.*', file_name, re.IGNORECASE) is not None:
            # parsed = []
            for ind, row in zip(data.index, data[q]):
                multi = re_multiValuedSearch.findall(row)
                row = 1
                new_column = ws.max_column +1
                for ii, m in enumerate(multi):
                    ws.cell(row=ii+1, column=new_column, value=m[0])
                new_column = ws.max_column +1
                for ii, m in enumerate(multi):
                    ws.cell(row=ii+1, column=new_column, value=m[1])

            ws.column_dimensions.width = 40
            wb.save('{}.xlsx'.format(file_name[:10]))
            # wb.save('{}.xlsx'.format(file_name[:10]))
            # questions = []
            # for i in parsed:
            #     for j in i:
            #         if j[0] not in questions:
            #             questions.append(j[0])



                # for i in question_set:
                # rows = data[q].tolist()
                # print('\n\n')
                # for row in rows:
                #     print(row)


                    #     print(i)
                    #     print('here')
                # break
                # text = data[q].max()
                # slash_count = text.count('/')
                # slash_pos = [pos for pos, char in enumerate(text) if char == '/']
                # colon_count = text.count(':')
                # colon_pos = [pos for pos, char in enumerate(text) if char == ':']
                # semicolon_count = text.count(';')
                # semicolon_pos = [pos for pos, char in enumerate(text) if char == ';']

                # poss = [pos for pos, char in enumerate(text) if char in [';',':','/']]
                #
                # pointer = []
                # for num, char in enumerate(text):
                #     if num in poss:
                #         pointer.append('^')
                #     else:
                #         pointer.append(' ')
                # pointer = ''.join(pointer)
                #
                # # total_character_count = slash_count + colon_count + semicolon_count
                # chunk_list = list(np.arange(0, len(text), 100))
                # if chunk_list[-1] != len(text):
                #     chunk_list.extend([len(text)])
                #
                # for ind, row in zip(data.index, data[q]):
                #     multi = re_multiValuedSearch.findall(row)
                # parentheses_pos = []
                # parentheses_pos_begin = []
                # parentheses_pos_end = []
                #
                # question_set = set()
                # for whole_regex in multi:
                #     parentheses_pos_begin.append(text.find(whole_regex[0]))
                #     parentheses_pos_end.append(text.find(whole_regex[0]) + len(whole_regex[0]))
                #     question_set.add(whole_regex[0])
                #
                # # print(parentheses_pos_begin)
                # # print(parentheses_pos_end)
                # for i in range(0, len(text)):
                #     if i not in parentheses_pos_begin and i not in parentheses_pos_end:
                #         parentheses_pos.insert(i, ' ')
                #     elif i in parentheses_pos_begin:
                #         parentheses_pos.append('(')
                #     elif i in parentheses_pos_end:
                #         parentheses_pos.append(')')
                #     else:
                #         print('error')
                # parentheses_pos = ''.join(parentheses_pos)
                # # print(parentheses_pos)
                # for i, j in zip(chunk_list[:-1],chunk_list[1:]):
                #     print(text[i:j])
                #     print(parentheses_pos[i:j])
                #
                #

    return data, questions_df, symbol



def opener(file):
    import csv
    from collections import OrderedDict
    # with open('columns.csv', encoding="utf8", mode='r') as csv_file:
    #     csv_reader = csv.reader(csv_file, delimiter=',')
    #     line_count = 0
    #     rows = []
    #     for row in csv_reader:
    #         line_count += 1
    #         rows.append(row)
    #     print(f'Processed {line_count} lines.')
    #     return rows
    try:
        columns = []
        with open(file, encoding = "utf8", mode='r') as f:
            reader = csv.reader(f)
            for row in reader:
                if columns:
                    row = [re.sub(r'\xa0', ' ', i) for i in row]
                    row = [re.sub('%%', '', i) for i in row]
                    row = [re.sub('\ufeff', '', i) for i in row]
                    for i, value in enumerate(row):
                        columns[i].append(value)
                else:
                    row = [re.sub(r'\xa0', ' ', i) for i in row]
                    row = [re.sub('%%', '', i) for i in row]
                    row = [re.sub('\ufeff', '', i) for i in row]
                    # first row
                    blank_column_count = 1
                    duplicate_column_count = 1
                    duplicate_tracker = []
                    for i, value in enumerate(row):
                        if value == '':
                            emptyColName = 'blank_column_{}'.format(blank_column_count)
                            row[i] = emptyColName
                            blank_column_count += 1
                            duplicate_tracker.append(emptyColName)
                        elif value in duplicate_tracker:
                            row[i] = value + '__{}'.format(duplicate_column_count)
                            duplicate_column_count += 1
                            duplicate_tracker.append(row[i])
                        else:
                            duplicate_tracker.append(value)
                            pass
                    columns = [[value] for value in row]
        as_dict = OrderedDict((subl[0], subl[1:]) for subl in columns)
        # print(as_dict.keys())
        return as_dict
    except:
        columns = []
        with open(file, encoding = "ISO-8859-1", mode='r') as f:
            reader = csv.reader(f)
            for row in reader:
                if columns:
                    row = [re.sub(r'\xa0', ' ', i) for i in row]
                    row = [re.sub('%%', '', i) for i in row]
                    row = [re.sub('\ufeff', '', i) for i in row]
                    for i, value in enumerate(row):
                        columns[i].append(value)
                else:
                    row = [re.sub(r'\xa0', ' ', i) for i in row]
                    row = [re.sub('%%', '', i) for i in row]
                    row = [re.sub('\ufeff', '', i) for i in row]
                    # first row
                    blank_column_count = 1
                    duplicate_column_count = 1
                    duplicate_tracker = []
                    for i, value in enumerate(row):
                        if value == '':
                            emptyColName = 'blank_column_{}'.format(blank_column_count)
                            row[i] = emptyColName
                            blank_column_count += 1
                            duplicate_tracker.append(emptyColName)
                        elif value in duplicate_tracker:
                            row[i] = value + '__{}'.format(duplicate_column_count)
                            duplicate_column_count += 1
                            duplicate_tracker.append(row[i])
                        else:
                            duplicate_tracker.append(value)
                            pass
                    columns = [[value] for value in row]

        as_dict = OrderedDict((subl[0], subl[1:]) for subl in columns)
        # return as_dict
    # print('\n\n\n')
    # print(as_dict.keys())
    # print('\n\n\n')
    return as_dict
def ppl_parser(df):
    # Create dataframe of respondents
    ppl = dict()
    cols = ['ResponseID', 'UniqueIdentifier', 'FirstName', 'LastName', 'MiddleName', 'Capacity', 'Audit', 'Compensation', 'NomGov', 'Title']
    cols = ['UniqueIdentifier', 'FirstName', 'LastName', 'MiddleName', 'Capacity', 'Audit', 'Compensation', 'NomGov', 'Title']
    for k, v in df.items():
        if k in cols:
            ppl[k] = v

    ppl_df = pd.DataFrame.from_dict(ppl)
    # if (list(ppl_df.columns)[0] == '\ufeffUniqueIdentifier') | ((list(ppl_df.columns)[0] == '\ufeffResponseID')):
    #     newFirstCol = re.sub('\ufeff', '', list(ppl_df.columns)[0])
    #     ppl_df = ppl_df.rename(columns={list(ppl_df.columns)[0]: newFirstCol})

    # if 'ResponseID' in ppl_df.columns:
    #     ppl_df.set_index('ResponseID', inplace=True)
    # elif  'UniqueIdentifier' in ppl_df.columns:
    #     ppl_df.set_index('UniqueIdentifier', inplace=True)
    if  'UniqueIdentifier' in ppl_df.columns:
        ppl_df.set_index('UniqueIdentifier', inplace=True)
    # ppl_df['test'] = [2,3,4,5,6,7]
    # ppl_df.set_index('test', inplace=True)
    # print(ppl_df.index)
    if None in set(ppl_df['Capacity'].tolist()) or '' in set(ppl_df['Capacity'].tolist()):
        logging.warning('Null values in Capacity column.\nppl_df will not be sorted.')
        return ppl_df

    orderedCapacities = ['Both', 'Director', 'Officer']
    df_list = []
    for i in orderedCapacities:
        d = ppl_df[ppl_df['Capacity']==i]
        d.sort_values(['LastName', 'FirstName'], inplace=True)
        df_list.append(d)
    ppl_df = pd.concat(df_list)

    return ppl_df

if __name__ == "__main__":
    start_time = time.time()
    print(sys.version)
    print('\n------------------------------------------\n')

    cwd = os.getcwd()
    cwd = cwd + r'\inputs'

    modified = re.compile('^[^~].*modified.*\.csv', re.IGNORECASE)
    results = openpyxl.Workbook()
    ws = results.active
    ws.cell(row=1, column=1, value='file')
    ws.cell(row=2, column=1, value='question')
    ws.cell(row=4, column=1, value='general match')
    ws.cell(row=5, column=1, value='row match')
    ws.cell(row=6, column=1, value='column match')
    ws.cell(row=7, column=1, value='no space match')
    ws.cell(row=8, column=1, value='wildcard match')

    for folder in os.listdir(cwd):
        sub_folder = cwd + r'\{}'.format(folder)
        for file in os.listdir(sub_folder):
            file_path = sub_folder + r'\{}'.format(file)
            if modified.match(file):
                # print('\n-----------------\nProccessing Modified...')
                # print('Modified File Name: ', file)
                mdf_raw = opener(file_path)
                mdf, mquestions_df, symbol  = parse_raw_fsr(mdf_raw, ws, file)

    # results.save('results.xlsx')
    #####################################
    print('\n\n--- Run Time ---')
    print('--- %s seconds ---' % (time.time() - start_time))
